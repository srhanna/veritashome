﻿
Public Class ForteParts
    Private sPartNo As String
    Private sVIN As String
    Private dRetailAmt As Double
    Private sPartDesc As String


    Public Property PartNo As String
        Get
            PartNo = sPartNo
        End Get
        Set(value As String)
            sPartNo = value
        End Set
    End Property

    Public Property VIN As String
        Get
            VIN = sVIN
        End Get
        Set(value As String)
            sVIN = value
        End Set
    End Property

    Public Property RetailAmt As Double
        Get
            RetailAmt = dRetailAmt
        End Get
        Set(value As Double)
            dRetailAmt = value
        End Set
    End Property

    Public Property PartDesc As String
        Get
            PartDesc = sPartDesc
        End Get
        Set(value As String)
            sPartDesc = value
        End Set
    End Property

    Public Sub ProcessPartNo()
        Dim sOut As String
        Dim fp As Object = New FortePartsAPI.FdsPartsAndLaborWebServicePortTypeClient
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim xmlD As New Xml.XmlDocument
        Dim xmlNR As Xml.XmlNodeReader

        sOut = fp.getPart("Forte 1.0", "CGAINTERNAL", "kuEOY8jtDNX7isCM", "", sVIN, sPartNo, "U", "1")
        sOut = "<Test>" & sOut & "</Test>"
        xmlD.LoadXml(sOut)
        xmlNR = New Xml.XmlNodeReader(xmlD)
        ds.ReadXml(xmlNR)
        If ds.Tables.Count = 1 Then
            dRetailAmt = 0
            sPartDesc = ""
            Exit Sub
        End If
        dt = ds.Tables(1)
        dr = dt.Rows(0)
        dRetailAmt = dr(2)
        sPartDesc = dr(4)
    End Sub
End Class
