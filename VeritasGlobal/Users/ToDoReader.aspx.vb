﻿Imports System.Configuration.ConfigurationManager

Public Class ToDoReader
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsMessage.ConnectionString = AppSettings("connstring")
        GetServerInfo()

        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            pnlDetail.Visible = False
            pnlList.Visible = True
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub rgToDoMessengerList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgToDoMessengerList.SelectedIndexChanged
        pnlDetail.Visible = True
        pnlList.Visible = False
        hfIDX.Value = rgToDoMessengerList.SelectedValue
        FillScreen()
    End Sub

    Private Sub FillScreen()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where idx = " & hfIDX.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfFromID.Value = clR.Fields("fromid")
            lblToDisplay.Text = GetUserName()
            lblFromDisplay.Text = GetFromName()
            lblDate.Text = clR.Fields("messagedate")
            txtHeader.Text = clR.Fields("header")
            txtMessageDisplay.Text = clR.Fields("message")
            If clR.Fields("completedmessage") Then
                btnCompleted.Visible = False
            End If
        End If
    End Sub

    Private Sub btnClosed_Click(sender As Object, e As EventArgs) Handles btnClosed.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgToDoMessengerList.Rebind()
    End Sub

    Private Sub btnCompleted_Click(sender As Object, e As EventArgs) Handles btnCompleted.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update usermessage "
        SQL = SQL + "set completedmessage = 1 "
        SQL = SQL + "where idx = " & hfIDX.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgToDoMessengerList.Rebind()
    End Sub

    Private Sub btnRemoveMessage_Click(sender As Object, e As EventArgs) Handles btnRemoveMessage.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update usermessage "
        SQL = SQL + "set deletemessage = 1 "
        SQL = SQL + "where idx = " & hfIDX.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgToDoMessengerList.Rebind()
    End Sub

    Private Function GetFromName()
        Dim SQL As String
        Dim clR As New clsDBO
        GetFromName = ""
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfFromID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetFromName = clR.Fields("fname") & " " & clR.Fields("lname")
        End If
    End Function

    Private Function GetUserName() As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetUserName = ""
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetUserName = clR.Fields("fname") & " " & clR.Fields("lname")

        End If
    End Function


End Class