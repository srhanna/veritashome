﻿Public Class AgentsNote
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsAgentNote.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()

        If Not IsPostBack Then
            hfToday.Value = Format(Today, "M/d/yyyy")
            hfAgentID.Value = Request.QueryString("AgentID")
            pnlControl.Visible = True
            pnlDetail.Visible = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub rgAgentNote_SelectedCellChanged(sender As Object, e As EventArgs) Handles rgAgentNote.SelectedCellChanged
        pnlControl.Visible = False
        pnlDetail.Visible = True
        hfAgentNoteID.Value = rgAgentNote.SelectedValue
        FillNote()
    End Sub

    Private Sub FillNote()
        Dim SQL As String
        Dim clRN As New clsDBO
        SQL = "select note, createdate, moddate, cre.email as CEmail, mod.email as MEMail from agentsnote rn "
        SQL = SQL + "left join userinfo cre on rn.createby = cre.userid "
        SQL = SQL + "left join userinfo mod on mod.userid = rn.modby "
        SQL = SQL + "where agentnoteid = " & hfAgentNoteID.Value

        clRN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRN.RowCount > 0 Then
            clRN.GetRow()
            txtNote.Text = clRN.Fields("Note")
            txtCreBy.Text = clRN.Fields("CEMail")
            txtCreDate.Text = clRN.Fields("createdate")
            txtModBy.Text = clRN.Fields("MEMail")
            txtModDate.Text = clRN.Fields("moddate")
            txtNote.Text = clRN.Fields("note")
            chkClaimAlways.Checked = CBool(clRN.Fields("claimalways"))
            chkClaimEntry.Checked = CBool(clRN.Fields("claimnewentry"))
            chkContractAlways.Checked = CBool(clRN.Fields("contractalways"))
            chkContractCancel.Checked = CBool(clRN.Fields("contractcancel"))
            chkContractNewEntry.Checked = CBool(clRN.Fields("ContractNewEntry"))
        Else
            txtNote.Text = ""
            txtCreBy.Text = ""
            txtCreDate.Text = ""
            txtModBy.Text = ""
            txtModDate.Text = ""
            txtNote.Text = ""
            chkContractNewEntry.Checked = False
            chkContractCancel.Checked = False
            chkContractAlways.Checked = False
            chkClaimEntry.Checked = False
            chkClaimAlways.Checked = False
        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlControl.Visible = True
        pnlDetail.Visible = False
        rgAgentNote.Rebind()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clRN As New clsDBO
        SQL = "select * from Agentsnote "
        SQL = SQL + "where agentnoteid = " & hfAgentNoteID.Value
        clRN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRN.RowCount = 0 Then
            clRN.NewRow()
            clRN.Fields("createby") = hfUserID.Value
            clRN.Fields("createdate") = Today
        Else
            clRN.GetRow()
            clRN.Fields("modby") = hfUserID.Value
            clRN.Fields("moddate") = Today
        End If
        clRN.Fields("note") = txtNote.Text
        clRN.Fields("agentid") = hfAgentID.Value
        clRN.Fields("claimalways") = chkClaimAlways.Checked
        clRN.Fields("claimnewentry") = chkClaimEntry.Checked
        clRN.Fields("contractalways") = chkContractAlways.Checked
        clRN.Fields("contractnewentry") = chkContractNewEntry.Checked
        clRN.Fields("contractcancel") = chkContractCancel.Checked
        If clRN.RowCount = 0 Then
            clRN.AddRow()
        End If
        clRN.SaveDB()
        pnlControl.Visible = True
        pnlDetail.Visible = False
        rgAgentNote.Rebind()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        hfAgentNoteID.Value = 0
        pnlControl.Visible = False
        pnlDetail.Visible = True
        FillNote()
    End Sub
End Class