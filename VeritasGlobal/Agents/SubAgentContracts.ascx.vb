﻿Public Class SubAgentContracts
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        hfSubAgentID.Value = Request.QueryString("subagentID")
    End Sub

    Private Sub rgSubAgentContracts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgSubAgentContracts.SelectedIndexChanged
        Response.Redirect("~/contract/contract.aspx?sid=" & hfID.Value & "&contractid=" & rgSubAgentContracts.SelectedValue)
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

End Class