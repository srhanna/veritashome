﻿Public Class AgentsModify
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsAgentStatus.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        hfAgentID.Value = Request.QueryString("AgentID")
        pnlModify.Visible = True
        pnlSearch.Visible = False
        If Not IsPostBack Then
            FillAgent()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillAgent()
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select * from agents where agentid = " & hfAgentID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            txtAddr1.Text = clA.Fields("addr1")
            txtAddr2.Text = clA.Fields("addr2")
            txtAgentName.Text = clA.Fields("agentname")
            txtAgentNo.Text = clA.Fields("agentno")
            txtCity.Text = clA.Fields("city")
            txtContact.Text = clA.Fields("contact")
            txtEIN.Text = clA.Fields("ein")
            txtPhone.Text = clA.Fields("Phone")
            txtEMail.Text = clA.Fields("email")
            cboAgentStatus.SelectedValue = clA.Fields("statusid")
            hfParentAgentID.Value = clA.Fields("parentagentid")
            txtParentAgent.Text = GetParentAgentInfo(clA.Fields("parentagentid"))
            cboState.SelectedValue = clA.Fields("state")
            txtZip.Text = clA.Fields("zip")
        Else
            txtAddr1.Text = ""
            txtAddr2.Text = ""
            txtAgentName.Text = ""
            txtAgentNo.Text = ""
            txtCity.Text = ""
            txtContact.Text = ""
            txtEIN.Text = ""
            txtPhone.Text = ""
            txtEMail.Text = ""
            hfParentAgentID.Value = 0
            txtParentAgent.Text = ""
            cboState.Text = ""
            txtZip.Text = ""
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        FillAgent()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clA As New clsDBO

        SQL = "select * from agents where agentid = " & hfAgentID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            clA.Fields("moddate") = Today
            clA.Fields("modby") = hfUserID.Value
        Else
            clA.NewRow()
            clA.Fields("credate") = Today
            clA.Fields("creby") = hfUserID.Value
        End If
        clA.Fields("addr1") = txtAddr1.Text
        clA.Fields("addr2") = txtAddr2.Text
        clA.Fields("city") = txtCity.Text
        clA.Fields("agentname") = txtAgentName.Text
        clA.Fields("agentno") = txtAgentNo.Text
        clA.Fields("contact") = txtContact.Text
        clA.Fields("phone") = txtPhone.Text
        clA.Fields("ein") = txtEIN.Text
        clA.Fields("email") = txtEMail.Text
        clA.Fields("parentagentid") = hfParentAgentID.Value
        clA.Fields("statusid") = cboAgentStatus.SelectedValue
        clA.Fields("state") = cboState.SelectedValue
        clA.Fields("zip") = txtZip.Text
        If clA.RowCount = 0 Then
            clA.AddRow()
        End If
        clA.SaveDB()
        redirectagent
    End Sub

    Private Sub RedirectAgent()
        If hfAgentID.Value <> "0" Then
            Response.Redirect("agents.aspx?sid=" & hfID.Value & "&AgentID=" & hfAgentID.Value)
        Else
            Response.Redirect("agents.aspx?sid=" & hfID.Value & "&AgentID=" & getagentid)
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        pnlModify.Visible = False
        pnlSearch.Visible = True
    End Sub

    Private Sub rgAgents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgents.SelectedIndexChanged
        hfParentAgentID.Value = rgAgents.SelectedValue
        txtParentAgent.Text = GetParentAgentInfo(hfParentAgentID.Value)
        pnlModify.Visible = True
        pnlSearch.Visible = False
    End Sub

    Private Function GetAgentID() As Long
        GetAgentID = 0
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select max(agentid) as agentid from agents "
        SQL = SQL + "where creby = " & hfUserID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            GetAgentID = clA.Fields("agentid")
        End If
    End Function

    Private Function GetParentAgentInfo(xParentAgentID As Long) As String
        GetParentAgentInfo = ""
        If xParentAgentID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select * from agents where agentid = " & xParentAgentID
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            GetParentAgentInfo = clA.Fields("agentno") & " / " & clA.Fields("agentname")
        End If
    End Function
End Class