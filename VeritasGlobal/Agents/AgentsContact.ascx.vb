﻿Public Class AgentsContact
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        If Not IsPostBack Then
            pnlControl.Visible = True
            pnlDetail.Visible = False
            hfAgentID.Value = Request.QueryString("AgentID")
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub rgAgentContact_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgentContact.SelectedIndexChanged
        pnlControl.Visible = False
        pnlDetail.Visible = True
        hfAgentContactID.Value = rgAgentContact.SelectedValue
        FillAgentContact()
    End Sub

    Private Sub FillAgentContact()
        Dim SQL As String
        Dim clAC As New clsDBO
        SQL = "select * from agentscontact "
        SQL = SQL + "where agentcontactid = " & hfAgentContactID.Value
        clAC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clAC.RowCount > 0 Then
            clAC.GetRow()
            txtEMail.Text = clAC.Fields("email")
            txtFName.Text = clAC.Fields("fname")
            txtLName.Text = clAC.Fields("lname")
            txtPhone.Text = clAC.Fields("phone")
            txtPosition.Text = clAC.Fields("position")
        Else
            txtEMail.Text = ""
            txtFName.Text = ""
            txtLName.Text = ""
            txtPhone.Text = ""
            txtPosition.Text = ""
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlControl.Visible = False
        pnlDetail.Visible = True
        hfAgentContactID.Value = 0
        FillAgentContact()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlControl.Visible = True
        pnlDetail.Visible = False
        rgAgentContact.Rebind()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clAC As New clsDBO
        SQL = "select * from agentscontact "
        SQL = SQL + "where agentcontactid = " & hfAgentContactID.Value
        clAC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clAC.RowCount = 0 Then
            clAC.NewRow()
        Else
            clAC.GetRow()
        End If
        clAC.Fields("agentid") = hfAgentID.Value
        clAC.Fields("fname") = txtFName.Text
        clAC.Fields("lname") = txtLName.Text
        clAC.Fields("phone") = txtPhone.Text
        clAC.Fields("email") = txtEMail.Text
        clAC.Fields("position") = txtPosition.Text
        If clAC.RowCount = 0 Then
            clAC.AddRow()
        End If
        clAC.SaveDB()
        pnlControl.Visible = True
        pnlDetail.Visible = False
        rgAgentContact.Rebind()
    End Sub
End Class