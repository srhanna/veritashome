﻿Imports System.Configuration.ConfigurationManager

Public Class ContractPayment
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SqlDataSource1.ConnectionString = AppSettings("connstring")
        SqlDataSource2.ConnectionString = AppSettings("connstring")
        GetServerInfo()
        hfContractID.Value = Request.QueryString("contractid")

        If Not IsPostBack Then
            ReadOnlyButtons()
            pnlPaymentList.Visible = True
            pnlPayment.Visible = False
            pnlPayeeSearch.Visible = False
            pnlPayeeModify.Visible = False
            fillpayment
        End If
    End Sub

    Private Sub FillPayment()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select contractpaymentid, cp.contractid, d.DealerName as Company, c.FName, c.lname, cp.paymenttype, cp.DatePaid, cp.checkno, 
        PaidAmt  from ContractPayment cp
        inner join dealer d on cp.PayeeID = d.DealerID
        inner join contract c on cp.ContractID = c.ContractID "
        SQL = SQL + "where cp.contractid = " & hfContractID.Value
        rgContractPayment.DataSource = clR.GetData(SQL, AppSettings("connstring"))
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnAddPayment.Enabled = False
                btnPayeeSearch.Enabled = False
                btnSavePayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnAddPayment_Click(sender As Object, e As EventArgs) Handles btnAddPayment.Click
        hfContractPaymentID.Value = 0
        txtpaidAmt.Text = ""
        txtCheckNo.Text = ""
        txtPayee.Text = ""
        hfPayeeID.Value = 0
        rdpProcessDate.Clear()
        pnlPaymentList.Visible = False
        pnlPayment.Visible = True
    End Sub

    Private Sub btnSavePayment_Click(sender As Object, e As EventArgs) Handles btnSavePayment.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from contractpayment "
        SQL = SQL + "where contractpaymentid = " & hfContractPaymentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("contractid") = hfContractID.Value
        clR.Fields("payeeid") = hfPayeeID.Value
        clR.Fields("paymenttype") = cboPaymentType.SelectedValue
        clR.Fields("datepaid") = rdpProcessDate.SelectedDate
        clR.Fields("checkno") = txtCheckNo.Text
        clR.Fields("paidamt") = txtpaidAmt.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
        pnlPayment.Visible = False
        pnlPaymentList.Visible = True
        rgContractPayment.Rebind()
        checkamt
    End Sub

    Private Sub CheckAmt()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dPayment As Double = 0
        Dim dCost As Double = 0
        SQL = "select dealercost from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dCost = clR.Fields("dealercost")
        End If
        SQL = "select sum(paidamt) as Amt from contractpayment "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dPayment = clR.Fields("amt")
        End If
        If dCost = dPayment Then
            SQL = "update contract "
            SQL = SQL + "set status = 'Paid' "
            SQL = SQL + "where contractid = " & hfContractID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
            Response.Redirect("~/contract/contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
        End If
    End Sub


    Private Sub btnCancelPayment_Click(sender As Object, e As EventArgs) Handles btnCancelPayment.Click
        pnlPayment.Visible = False
        pnlPaymentList.Visible = True
        FillPayment()
    End Sub

    Private Sub btnPayeeSearch_Click(sender As Object, e As EventArgs) Handles btnPayeeSearch.Click
        pnlPayment.Visible = False
        pnlPayeeSearch.Visible = True
    End Sub

    Private Sub btnCancelSearch_Click(sender As Object, e As EventArgs) Handles btnCancelSearch.Click
        pnlPayment.Visible = True
        pnlPayeeSearch.Visible = False
    End Sub

    Private Sub rgPayee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgPayee.SelectedIndexChanged
        hfPayeeID.Value = rgPayee.SelectedValue
        FillPayee()
        pnlPayeeSearch.Visible = False
        pnlPayment.Visible = True
    End Sub

    Private Sub FillPayee()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtPayee.Text = clR.Fields("dealerno") & " / " & clR.Fields("dealername")
        End If
    End Sub

    Private Sub rgContractPayment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgContractPayment.SelectedIndexChanged
        hfContractPaymentID.Value = rgContractPayment.SelectedValue
        pnlPaymentList.Visible = False
        pnlPayment.Visible = True
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from contractpayment "
        SQL = SQL + "where contractpaymentid = " & hfContractPaymentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtCheckNo.Text = clR.Fields("checkno")
            hfPayeeID.Value = clR.Fields("payeeid")
            FillPayee()
            rdpProcessDate.SelectedDate = clR.Fields("datepaid")
            txtpaidAmt.Text = clR.Fields("paidamt")
        End If
    End Sub
End Class