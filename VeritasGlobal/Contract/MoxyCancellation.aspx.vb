﻿Public Class MoxyCancellation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            FillContract()
            FillMoxyCancellation()
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub FillMoxyCancellation()
        Dim SQL As String
        Dim clCCM As New clsDBO
        SQL = "select * from contractcancelmoxy "
        SQL = SQL + "where contractid = " & hfContractID.Value & " "
        SQL = SQL + "and cancelstatus = 'Cancelled' "
        clCCM.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCCM.RowCount > 0 Then
            clCCM.GetRow()
            If clCCM.Fields("canceleffdate").Length > 0 Then
                rdpProcessDate.SelectedDate = CDate(clCCM.Fields("canceleffdate"))
            End If
            txtMiles.Text = Format(CLng(clCCM.Fields("cancelmile")), "#,##0")
            txtCancelPercent.Text = Format(CDbl(clCCM.Fields("cancelfactor")), "Percent")
            txtTermPercent.Text = Format(CDbl(clCCM.Fields("termfactor")), "Percent")
            txtMilePercent.Text = Format(CDbl(clCCM.Fields("milefactor")), "Percent")
            txtCancelStatus.Text = clCCM.Fields("cancelstatus")
            txtGrossCustomer.Text = Format(CDbl(clCCM.Fields("grosscustomer")), "currency")
            txtGrossDealer.Text = Format(CDbl(clCCM.Fields("grossdealer")), "currency")
            txtGrossAdmin.Text = Format(CDbl(clCCM.Fields("grossadmin")), "currency")
            txtClaimAmt.Text = Format(CDbl(clCCM.Fields("claimamt")), "currency")
            txtCancelFee.Text = Format(CDbl(clCCM.Fields("cancelfeeamt")), "currency")
            txtQuoteDate.Text = Format(CDate(clCCM.Fields("canceldate")), "M/d/yyyy")
            txtRequestDate.Text = Format(CDate(clCCM.Fields("canceldate")), "M/d/yyyy")
            txtCancelDate.Text = Format(CDate(clCCM.Fields("canceldate")), "M/d/yyyy")
            txtCustomerRefund.Text = Format(CDbl(clCCM.Fields("adminamt")) + CDbl(clCCM.Fields("dealeramt")), "currency")
            txtFromDealer.Text = Format(CDbl(clCCM.Fields("dealeramt")), "currency")
            txtFromAdmin.Text = Format(CDbl(clCCM.Fields("adminamt")), "currency")
        End If
    End Sub

    Private Sub FillContract()
        Dim SQL As String
        Dim sTemp As String
        SQL = "select * from contract c "
        SQL = SQL + "left join program p on c.programid = p.programid "
        SQL = SQL + "left join plantype pt on pt.plantypeid = c.plantypeid "
        SQL = SQL + "where contractid = " & hfContractID.Value
        Dim clC As New clsDBO
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            lblContractNo.Text = clC.Fields("contractno")
            lblStatus.Text = clC.Fields("status")
            If clC.Fields("saledate").Length > 0 Then
                lblSaleDate.Text = Format(CDate(clC.Fields("saledate")), "M/d/yyyy")
            End If
            If clC.Fields("effdate").Length > 0 Then
                lblEffDate.Text = Format(CDate(clC.Fields("effdate")), "M/d/yyyy")
            End If
            If clC.Fields("expdate").Length > 0 Then
                lblExpDate.Text = Format(CDate(clC.Fields("expdate")), "M/d/yyyy")
            End If
            If clC.Fields("salemile").Length > 0 Then
                lblSaleMiles.Text = Format(CLng(clC.Fields("salemile")), "#,##0")
            End If
            If clC.Fields("effmile").Length > 0 Then
                lblEffMile.Text = Format(CLng(clC.Fields("effmile")), "#,##0")
            End If
            If clC.Fields("expmile").Length > 0 Then
                lblExpMile.Text = Format(CLng(clC.Fields("expmile")), "#,##0")
            End If
            lblProgram.Text = clC.Fields("programname")
            lblPlanType.Text = clC.Fields("plantype")
            lblTerm.Text = clC.Fields("termmonth") & "/" & Format(CLng(clC.Fields("termmile")), "#,##0")
            sTemp = clC.Fields("fname")
            sTemp = sTemp + clC.Fields("lname") & vbCrLf
            sTemp = sTemp + clC.Fields("addr1") & vbCrLf
            If clC.Fields("addr2").Length > 0 Then
                sTemp = sTemp + clC.Fields("addr2") & vbCrLf
            End If
            sTemp = sTemp + clC.Fields("city") & " "
            sTemp = sTemp + clC.Fields("state") & " "
            sTemp = sTemp + clC.Fields("zip") & vbCrLf
            sTemp = sTemp + clC.Fields("phone")
            txtCustomerInfo.Text = sTemp
            txtLienholder.Text = clC.Fields("lienholder")
            If clC.Fields("datepaid").Length > 0 Then
                lblPaidDate.Text = Format(CDate(clC.Fields("datepaid")), "M/d/yyyy")
            End If
            rbDealer.Checked = True
            GetDealerInfo(clC.Fields("dealerid"))
            GetAgentInfo(clC.Fields("agentsid"))
        End If
        hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" & hfContractID.Value
    End Sub

    Private Sub GetAgentInfo(xAgentID As Long)
        Dim clA As New clsDBO
        Dim SQL As String
        Dim sTemp As String
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & xAgentID
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            sTemp = clA.Fields("agentno") & vbCrLf
            sTemp = sTemp + clA.Fields("agentname") & vbCrLf
            sTemp = sTemp + clA.Fields("addr1") & vbCrLf
            If clA.Fields("addr2").Length > 0 Then
                sTemp = sTemp + clA.Fields("addr2") & vbCrLf
            End If
            sTemp = sTemp + clA.Fields("city") & " " & clA.Fields("state") & " " & clA.Fields("zip") & vbCrLf
            sTemp = sTemp + clA.Fields("phone")
            txtAgent.Text = sTemp
        End If
    End Sub

    Private Sub GetDealerInfo(xDealerID As Long)
        Dim clD As New clsDBO
        Dim SQL As String
        Dim sTemp As String
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & xDealerID
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            sTemp = clD.Fields("dealerno") & vbCrLf
            sTemp = sTemp + clD.Fields("dealername") & vbCrLf
            sTemp = sTemp + clD.Fields("Addr1") & vbCrLf
            If clD.Fields("Addr2").Length > 0 Then
                sTemp = sTemp + clD.Fields("Addr2") & vbCrLf
            End If
            sTemp = sTemp + clD.Fields("City") & " "
            sTemp = sTemp + clD.Fields("State") & " "
            sTemp = sTemp + clD.Fields("Zip") & vbCrLf
            sTemp = sTemp + clD.Fields("Phone")
            txtDealer.Text = sTemp
            If clD.Fields("dealerstatusid") = 5 Then
                rbOther.Checked = True
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        hfContractID.Value = Request.QueryString("contractid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnVerify_Click(sender As Object, e As EventArgs) Handles btnVerify.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        CalcCancellation(True)
        GetLienholder2()
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount = 0 Then
            clCC.NewRow()
        Else
            clCC.GetRow()
        End If
        Dim clP As New VeritasGlobalTools.clsPayee
        If rbCustomer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddCustomerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbDealer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddDealerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbOther.Checked = True Then
            clP.FName = txtFName.Text
            clP.CompanyName = txtCompanyName.Text
            clP.LName = txtLName.Text
            clP.Addr1 = txtAddr1.Text
            clP.Addr2 = txtAddr2.Text
            clP.City = txtCity.Text
            clP.State = cboState.Text
            clP.Zip = txtZip.Text
            clP.Phone = txtPhone.Text
            clP.AddOtherToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If clCC.RowCount = 0 Then
            clCC.Fields("credate") = Today
            clCC.Fields("creby") = hfUserID.Value
            clCC.AddRow()
        Else
            clCC.Fields("moddate") = Today
            clCC.Fields("modby") = hfUserID.Value
        End If
        clCC.SaveDB()
    End Sub

    Private Sub GetLienholder2()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtLienholder.Text = clC.Fields("lienholder")
        End If
    End Sub

    Private Sub CalcCancellation(xQuote As Boolean)
        pnlVerify.Visible = True
        Dim clCancel As New VeritasGlobalTools.clsCancellation
        clCancel.CancelDate = rdpProcessDate.SelectedDate
        clCancel.ContractID = hfContractID.Value
        clCancel.Quote = xQuote
        clCancel.CalculateCancellation()
        txtCancelFeeV.Text = Format(clCancel.CancelFee, "#,##0.00")
        txtCancelPercentV.Text = Format(clCancel.CancelPer, "0.###%")
        txtClaimAmtV.Text = Format(clCancel.Claims, "#,##0.00")
        txtCustomerRefundV.Text = Format(clCancel.DealerAmt + clCancel.AdminAmt, "#,##0.00")
        txtFromAdminV.Text = Format(clCancel.AdminAmt, "#,##0.00")
        txtFromDealerV.Text = Format(clCancel.DealerAmt, "#,##0.00")
        txtTermPercentV.Text = Format(clCancel.CancelTermPer, "0.###%")
        txtGrossCustomerV.Text = Format(clCancel.GrossCustomer, "#,##0.00")
        txtGrossDealerV.Text = Format(clCancel.GrossDealer, "#,##0.00")
        txtGrossAdminV.Text = Format(clCancel.GrossAdmin, "#,##0.00")
    End Sub

    Private Sub btnDeny_Click(sender As Object, e As EventArgs) Handles btnDeny.Click
        updateprocess
        dopopup
    End Sub

    Private Sub DoPopup()
        RadWindowManager1.RadAlert("Contract in Moxy needs to be placed in Sold Status", 400, 100, "Deny", "")
    End Sub

    Private Sub UpdateProcess()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "update contractcancelmoxy "
        SQL = SQL + "set processedcancel = 1 "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        MoveMOxyCancellation()
        UpdateProcess()
        RadWindowManager1.RadAlert("Accept is complete", 400, 100, "Accept", "")
    End Sub

    Private Sub MoveMOxyCancellation()
        Dim clCCM As New clsDBO
        Dim clCC As New clsDBO
        Dim clP As New VeritasGlobalTools.clsPayee
        Dim SQL As String
        SQL = "select * from contractcancelmoxy "
        SQL = SQL + "where contractid = " & Request.QueryString("contractid")
        clCCM.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCCM.RowCount > 0 Then
            clCCM.GetRow()
            Sql = "select * from contractcancel "
            SQL = SQL + "where contractid = " & Request.QueryString("contractid")
            clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clCC.RowCount > 0 Then
                clCC.GetRow()
            Else
                clCC.NewRow()
            End If
            clCC.Fields("contractid") = Request.QueryString("contractid")
            clCC.Fields("cancelstatus") = clCCM.Fields("cancelstatus")
            clCC.Fields("cancelmile") = clCCM.Fields("cancelmile")
            clCC.Fields("termfactor") = clCCM.Fields("termfactor")
            clCC.Fields("milefactor") = clCCM.Fields("milefactor")
            clCC.Fields("claimamt") = clCCM.Fields("claimamt")
            clCC.Fields("cancelfactor") = clCCM.Fields("cancelfactor")
            clCC.Fields("quotedate") = clCCM.Fields("canceldate")
            clCC.Fields("requestdate") = clCCM.Fields("canceldate")
            clCC.Fields("canceleffdate") = clCCM.Fields("canceleffdate")
            clCC.Fields("adminamt") = clCCM.Fields("adminamt")
            clCC.Fields("dealeramt") = clCCM.Fields("dealeramt")
            clCC.Fields("cancelfeeamt") = clCCM.Fields("cancelfeeamt")
            clCC.Fields("creby") = clCCM.Fields("creby")
            clCC.Fields("credate") = clCCM.Fields("credate")
            If rbCustomer.Checked = True Then
                clP.ContractID = hfContractID.Value
                clP.AddCustomerToPayee()
                clCC.Fields("payeeid") = clP.PayeeID
            End If
            If rbDealer.Checked = True Then
                clP.ContractID = hfContractID.Value
                clP.AddDealerToPayee()
                clCC.Fields("payeeid") = clP.PayeeID
            End If
            If rbOther.Checked = True Then
                clP.FName = txtFName.Text
                clP.CompanyName = txtCompanyName.Text
                clP.LName = txtLName.Text
                clP.Addr1 = txtAddr1.Text
                clP.Addr2 = txtAddr2.Text
                clP.City = txtCity.Text
                clP.State = cboState.Text
                clP.Zip = txtZip.Text
                clP.Phone = txtPhone.Text
                clP.AddOtherToPayee()
                clCC.Fields("payeeid") = clP.PayeeID
            End If
            clCC.Fields("dealer") = rbDealer.Checked
            clCC.Fields("Customer") = rbCustomer.Checked
            clCC.Fields("Other") = rbCustomer.Checked
            clCC.Fields("grosscustomer") = clCCM.Fields("grosscustomer")
            clCC.Fields("grossadmin") = clCCM.Fields("grossadmin")
            clCC.Fields("grossdealer") = clCCM.Fields("grossdealer")
            If clCC.RowCount = 0 Then
                clCC.AddRow()
            End If
            clCC.SaveDB()
        End If
        SQL = "update contract "
        SQL = SQL + "set status = 'Cancelled' "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        Response.Redirect("~/contract/MoxyCancellation.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
    End Sub

    Private Sub rbOther_CheckedChanged(sender As Object, e As EventArgs) Handles rbOther.CheckedChanged
        If rbOther.Checked Then
            pnlOther.Visible = True
        Else
            pnlOther.Visible = False
        End If
    End Sub
End Class