﻿Public Class ContractCommission
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsAgent.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsRateType.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        If Not IsPostBack Then
            hfContractID.Value = Request.QueryString("contractid")
            GetServerInfo()
            FillTable()
            ReadOnlybuttons
            pnlSearchRateType.Visible = False
            pnlChange.Visible = False
            pnlSearchAgent.Visible = False
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnAdd.Enabled = False
                btnSave.Enabled = False
                btnRateTypeSearch.Enabled = False
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillTable()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select contractcommissionid, companyname, rt.ratetypename,amt, datepaid, cancelamt, canceldate from contractcommissions cc  "
        SQL = SQL + "inner join ratetype rt on rt.ratetypeid = cc.ratetypeid "
        SQL = SQL + "inner join Payee p on p.PayeeID = cc.payeeid "
        SQL = SQL + "where cc.contractid = " & hfContractID.Value
        rgCommission.DataSource = clC.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgCommission.DataBind()
    End Sub

    Private Sub rgCommission_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgCommission.SelectedIndexChanged
        Dim clC As New clsDBO
        Dim SQL As String
        pnlCommission.Visible = False
        pnlChange.Visible = True
        SQL = "select * from contractcommissions "
        SQL = SQL + "where contractcommissionid = " & rgCommission.SelectedValue
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfContractCommissionID.Value = rgCommission.SelectedValue
            hfRateTypeID.Value = clC.Fields("ratetypeid")
            If clC.Fields("datepaid").Length > 0 Then
                rdpCycleDate.SelectedDate = clC.Fields("datepaid")
            Else
                rdpCycleDate.Clear()
            End If
            hfAgentID.Value = clC.Fields("payeeid")
            txtPayeeID.Text = GetPayee(hfAgentID.Value)
            txtAmt.Text = Format(CDbl(clC.Fields("amt")), "#,##0.00")
            If clC.Fields("canceldate").Length > 0 Then
                rdpCancelCycleDate.SelectedDate = clC.Fields("canceldate")
            Else
                rdpCancelCycleDate.Clear()
            End If
            txtCancelAmt.Text = clC.Fields("cancelamt")
            GetRateType()
        End If
    End Sub

    Private Sub GetRateType()
        Dim SQL As String
        Dim clRT As New clsDBO
        SQL = "select * from ratetype "
        SQL = SQL + "where ratetypeid = " & hfRateTypeID.Value
        clRT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRT.RowCount > 0 Then
            clRT.GetRow()
            txtRateType.Text = clRT.Fields("ratetypename")
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlChange.Visible = False
        pnlCommission.Visible = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        SQL = "select * from contractcommissions "
        SQL = SQL + "where contractcommissionid = " & hfContractCommissionID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount = 0 Then
            clCC.NewRow()
        Else
            clCC.GetRow()
        End If
        clCC.Fields("contractid") = hfContractID.Value
        clCC.Fields("ratetypeid") = hfRateTypeID.Value
        clCC.Fields("payeeid") = hfAgentID.Value
        If txtAmt.Text.Length > 0 Then
            clCC.Fields("amt") = CDbl(txtAmt.Text)
        Else
            clCC.Fields("amt") = 0
        End If
        If Not rdpCycleDate.SelectedDate Is Nothing Then
            clCC.Fields("datepaid") = rdpCycleDate.SelectedDate
        End If
        If txtCancelAmt.Text.Length > 0 Then
            clCC.Fields("cancelamt") = txtCancelAmt.Text
        Else
            clCC.Fields("cancelamt") = 0
        End If
        If Not rdpCancelCycleDate.SelectedDate Is Nothing Then
            clCC.Fields("canceldate") = txtCancelAmt.Text
        End If
        If clCC.RowCount = 0 Then
            clCC.AddRow()
        End If
        clCC.SaveDB()
        pnlChange.Visible = False
        pnlCommission.Visible = True
        FillTable()
    End Sub

    Public Sub GetAgentid()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfAgentID.Value = clC.Fields("agentsid")
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlCommission.Visible = False
        pnlChange.Visible = True
        hfContractCommissionID.Value = 0
        hfAgentID.Value = 0
        hfRateTypeID.Value = 0
        txtPayeeID.Text = ""
        txtRateType.Text = ""
        txtAmt.Text = ""
        rdpCycleDate.Clear()
        txtCancelAmt.Text = ""
        rdpCancelCycleDate.Clear()
    End Sub

    Private Sub btnRateTypeSearch_Click(sender As Object, e As EventArgs) Handles btnRateTypeSearch.Click
        rgRateType.Rebind()
        pnlChange.Visible = False
        pnlSearchRateType.Visible = True
    End Sub

    Private Sub btnCancelRateType_Click(sender As Object, e As EventArgs) Handles btnCancelRateType.Click
        pnlChange.Visible = True
        pnlSearchRateType.Visible = False
    End Sub

    Private Sub rgRateType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgRateType.SelectedIndexChanged
        hfRateTypeID.Value = rgRateType.SelectedValue
        pnlChange.Visible = True
        pnlSearchRateType.Visible = False
        GetRateType()
    End Sub

    Private Sub btnAgentSearch_Click(sender As Object, e As EventArgs) Handles btnAgentSearch.Click
        pnlChange.Visible = False
        pnlSearchAgent.Visible = True

    End Sub

    Private Sub btnClearAgentInfo_Click(sender As Object, e As EventArgs) Handles btnClearAgentInfo.Click
        hfAgentID.Value = 0
        txtPayeeID.Text = ""
        pnlChange.Visible = True
        pnlSearchAgent.Visible = False
    End Sub

    Private Sub rgAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgent.SelectedIndexChanged
        hfAgentID.Value = rgAgent.SelectedValue
        txtPayeeID.Text = GetPayee(hfAgentID.Value)
        pnlChange.Visible = True
        pnlSearchAgent.Visible = False
    End Sub
End Class