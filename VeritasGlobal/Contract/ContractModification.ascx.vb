﻿Imports System.Configuration.ConfigurationManager

Public Class ContractModification
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfContractID.Value = Request.QueryString("contractid")
            GetServerInfo()
            ReadOnlyButtons()
            fillhistory
        End If
    End Sub

    Private Sub FillHistory()
        Dim clH As New clsDBO
        Dim SQL As String
        SQL = "select ContractHistoryID, ContractID, fieldname, oldvalue, newvalue, credate, username from contracthistory ch
        inner join UserInfo ui on ui.userid = ch.CreBy "
        SQL = SQL + "where contractid = " & hfContractID.Value & " "
        SQL = SQL + "order by credate desc "
        rgHistory.DataSource = clH.GetData(SQL, AppSettings("connstring"))
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If Not CBool(clR.Fields("contractmodification")) Then
                btnUpdateSaleDate.Enabled = False
                'btnSave.Visible = False
            End If
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnUpdateSaleDate_Click(sender As Object, e As EventArgs) Handles btnUpdateSaleDate.Click
        Dim SQL As String
        Dim clC As New clsDBO
        Dim lDayDiff As Long
        If txtNewSaleDate.Text.Length = 0 Then
            Exit Sub
        End If
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            lDayDiff = DateDiff(DateInterval.Day, CDate(clC.Fields("saledate")), CDate(clC.Fields("effdate")))
            UpdateHistory("SaleDate", clC.Fields("saledate"), txtNewSaleDate.Text)
            clC.Fields("saledate") = txtNewSaleDate.Text
            clC.Fields("effdate") = DateAdd(DateInterval.Day, lDayDiff, CDate(clC.Fields("saledate")))
            clC.Fields("expdate") = DateAdd(DateInterval.Month, CLng(clC.Fields("termmonth")), CDate(clC.Fields("effdate")))
            clC.SaveDB()
            Response.Redirect("contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
        End If

    End Sub

    Private Sub UpdateHistory(xFieldName As String, xOldValue As String, xNewValue As String)
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contracthistory "
        SQL = SQL + "where contracthistoryid =  0 "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        clC.NewRow()
        clC.Fields("contractid") = hfContractID.Value
        clC.Fields("fieldname") = xFieldName
        clC.Fields("OldValue") = xOldValue
        clC.Fields("newvalue") = xNewValue
        clC.Fields("creby") = hfUserID.Value
        clC.Fields("credate") = Today
        clC.AddRow()
        clC.SaveDB()
    End Sub

End Class