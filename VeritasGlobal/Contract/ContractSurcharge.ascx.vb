﻿Imports System.Configuration.ConfigurationManager

Public Class ContractSurcharge
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetServerInfo()
            hfContractID.Value = Request.QueryString("contractid")
            FillSurchargeGrid()
        End If

    End Sub

    Private Sub FillSurchargeGrid()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select ContractSurchargeID, surcharge from contractSurcharge cs
                inner join Surcharge s on s.SurchargeID = cs.surchargeid "
        SQL = SQL + "where contractid = " & hfContractID.Value
        rgSurcharge.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgSurcharge.DataBind()
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub


End Class