﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContractCommission.ascx.vb" Inherits="VeritasGlobal.ContractCommission" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel runat="server" ID="pnlCommission">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnAdd" runat="server" Text="Add" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <telerik:RadGrid ID="rgCommission" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true">
        <ClientSettings EnablePostBackOnRowClick="true">
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <MasterTableView AutoGenerateColumns="false" DataKeyNames="ContractCommissionID" PageSize="10" ShowFooter="true">
            <Columns>
                <telerik:GridBoundColumn DataField="ContractCommissionID" UniqueName="ContractsCommissionID" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CompanyName" UniqueName="AgentName" HeaderText="Company Name"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateTypeName" HeaderText="Rate Type Name"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" DataFormatString="{0:c}" HeaderText="Amt"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" DataFormatString="{0:M/d/yyyy}" HeaderText="Paid Date"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CancelAmt" UniqueName="CancelAmt" DataFormatString="{0:c}" HeaderText="Cancel Amt"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CancelDate" UniqueName="CancelDate" DataFormatString="{0:M/d/yyyy}" HeaderText="Cancel Date"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Panel>
<asp:Panel runat="server" ID="pnlChange">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                Rate Type:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtRateType" runat="server"></asp:TextBox>
                <asp:Button ID="btnRateTypeSearch" runat="server" Text="Bucket Search" BackColor="#1eabe2" />
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                Payee Name:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPayeeID" runat="server"></asp:TextBox>
                <asp:Button ID="btnAgentSearch" runat="server" Text="Payee Search" BackColor="#1eabe2" />
            </asp:TableCell>
            <asp:TableCell>
                Cycle Date:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadDatePicker ID="rdpCycleDate" runat="server"></telerik:RadDatePicker>
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                Amt:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadNumericTextBox ID="txtAmt" runat="server"></telerik:RadNumericTextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Cancel Cycle Date:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadDatePicker ID="rdpCancelCycleDate" runat="server"></telerik:RadDatePicker>
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                Amt:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadNumericTextBox ID="txtCancelAmt" runat="server"></telerik:RadNumericTextBox>
            </asp:TableCell>
            <asp:TableCell>
                &nbsp
            </asp:TableCell>
            <asp:TableCell>
                &nbsp
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="#1eabe2"/>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="#1eabe2"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlSearchRateType" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnAddRateType" runat="server" Text="Add Bucket" BackColor="#1eabe2" BorderColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgRateType" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsRateType">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="RateTypeID" PageSize="10" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="RateTypeID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateType" HeaderText="Bucket" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CategoryName" UniqueName="RateCategory" HeaderText="Bucket Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="dsRateType"
                ProviderName="System.Data.SqlClient" SelectCommand="select ratetypeid, ratetypeName, categoryname from ratetype rt inner join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid where ratetypeid <> 29 " 
                runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Button ID="btnCancelRateType" runat="server" Text="Cancel" BackColor="#1eabe2" BorderColor="#1eabe2"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlSearchAgent" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgAgent" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsAgent">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="PayeeID" PageSize="10" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="PayeeID" ReadOnly="true" Visible="false" UniqueName="PayeeID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PayeeNo" UniqueName="AgentNo" HeaderText="Payee No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CompanyName" UniqueName="AgentName" HeaderText="Company Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DBA" UniqueName="DBA" HeaderText="DBA" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="dsAgent"
                ProviderName="System.Data.SqlClient" SelectCommand="select payeeid, payeeno, companyname, DBA, city, state from payee" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Button ID="btnClearAgentInfo" runat="server" Text="Clear Agent Info" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>

<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfContractCommissionID" runat="server" />
<asp:HiddenField ID="hfRateTypeID" runat="server" />
<asp:HiddenField ID="hfAgentID" runat="server" />
<asp:HiddenField ID="hfSubAgentID" runat="server" />
