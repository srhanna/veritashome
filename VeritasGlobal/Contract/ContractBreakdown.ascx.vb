﻿Public Class ContractBreakdown
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsRateCategory.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsRateType.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            hfContractID.Value = Request.QueryString("contractid")
            pnlAddRateType.Visible = False
            pnlModify.Visible = False
            pnlSearchRateType.Visible = False
            FillRateTypeGrid()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillRateTypeGrid()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select contractamtid, ratetypename, categoryname, amt, cancelamt from contractamt ca "
        SQL = SQL + "left join ratetype rt on rt.ratetypeid = ca.ratetypeid "
        SQL = SQL + "left join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid "
        SQL = SQL + "where contractid = " & hfContractID.Value & " "
        SQL = SQL + "order by orderid, ratetypename "

        rgAmount.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgAmount.DataBind()
    End Sub

    Private Sub rgAmount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAmount.SelectedIndexChanged
        hfContractAmtID.Value = rgAmount.SelectedValue
        If CheckCommission() Then
            Exit Sub
        End If
        pnlAmounts.Visible = False
        pnlModify.Visible = True
        FillModify()
    End Sub

    Private Sub FillModify()
        Dim SQL As String
        Dim clCA As New clsDBO
        SQL = "select * from contractamt ca "
        SQL = SQL + "left join ratetype rt on rt.ratetypeid = ca.ratetypeid "
        SQL = SQL + "where contractamtid = " & hfContractAmtID.Value
        clCA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCA.RowCount > 0 Then
            clCA.GetRow()
            hfRateTypeID.Value = clCA.Fields("ratetypeid")
            txtRateType.Text = clCA.Fields("ratetypename")
            txtAmount.Text = clCA.Fields("amt")
            txtCancelAmt.Text = clCA.Fields("cancelamt")
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlModify.Visible = False
        pnlAmounts.Visible = True
    End Sub

    Private Function CheckCommission() As Boolean
        CheckCommission = False
        Dim clCA As New clsDBO
        Dim SQL As String
        SQL = "select * from contractamt ca "
        SQL = SQL + "left join ratetype rt on rt.ratetypeid = ca.ratetypeid "
        SQL = SQL + "where contractamtid  = " & hfContractAmtID.Value
        clCA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCA.RowCount > 0 Then
            clCA.GetRow()
            If clCA.Fields("ratecategoryid") = 2 Then
                CheckCommission = True
                Exit Function
            End If
        End If
    End Function
End Class