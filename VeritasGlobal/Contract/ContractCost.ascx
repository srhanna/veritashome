﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContractCost.ascx.vb" Inherits="VeritasGlobal.ContractCost" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Dealer Cost:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtDealerCost" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Markup:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtMarkup" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Discount Amount:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtDiscountAmt" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Additional Markup:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtAddMarkup" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Customer Cost:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtCustomerCost" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />

