﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager
Imports Telerik.Windows.Documents.Spreadsheet.Expressions.Functions

Public Class contract
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            hfContractID.Value = Request.QueryString("contractid")
            FillContract()
            pvSurcharge.Selected = True
            pnlContractConfirm.Visible = False
        End If
        If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
        Else
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub FillContract()
        Dim SQL As String
        Dim sTemp As String
        SQL = "select p.programname as PName, * from contract c "
        SQL = SQL + "left join program p on c.programid = p.programid "
        SQL = SQL + "left join plantype pt on pt.plantypeid = c.plantypeid "
        SQL = SQL + "where contractid = " & hfContractID.Value
        Dim clC As New clsDBO
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            lblContractNo.Text = clC.Fields("contractno")
            lblStatus.Text = clC.Fields("status")
            If clC.Fields("saledate").Length > 0 Then
                lblSaleDate.Text = Format(CDate(clC.Fields("saledate")), "M/d/yyyy")
            End If
            If clC.Fields("effdate").Length > 0 Then
                lblEffDate.Text = Format(CDate(clC.Fields("effdate")), "M/d/yyyy")
            End If
            If clC.Fields("expdate").Length > 0 Then
                lblExpDate.Text = Format(CDate(clC.Fields("expdate")), "M/d/yyyy")
            End If
            hfProgramID.Value = clC.Fields("programid")
            hfPlanTypeID.Value = clC.Fields("plantypeid")
            lblProgram.Text = clC.Fields("pname")
            lblPlanType.Text = clC.Fields("plantype")
            lblTerm.Text = clC.Fields("termmonth")
            lblLienholder.Text = clC.Fields("lienholder")
            sTemp = clC.Fields("fname") & " "
            sTemp = sTemp + clC.Fields("lname") & vbCrLf
            sTemp = sTemp + clC.Fields("addr1") & vbCrLf
            If clC.Fields("addr2").Length > 0 Then
                sTemp = sTemp + clC.Fields("addr2") & vbCrLf
            End If
                sTemp = sTemp + clC.Fields("city") & " "
                sTemp = sTemp + clC.Fields("state") & " "
            sTemp = sTemp + clC.Fields("zip") & vbCrLf
            sTemp = sTemp + clC.Fields("phone")
            txtCustomerInfo.Text = sTemp
            sTemp = clC.Fields("coveredaddr1") & vbCrLf
            If clC.Fields("coveredaddr2").Length > 0 Then
                sTemp = sTemp + clC.Fields("coveredaddr2") & vbCrLf
            End If
            sTemp = sTemp + clC.Fields("city") & " "
            sTemp = sTemp + clC.Fields("state") & " "
            sTemp = sTemp + clC.Fields("zip")
            txtCoveredInfo.Text = sTemp

            If clC.Fields("datepaid").Length > 0 Then
                    lblPaidDate.Text = Format(CDate(clC.Fields("datepaid")), "M/d/yyyy")
                End If
                If clC.Fields("decpage").Length = 0 Then
                    tcDec.Visible = False
                    tcDesc2.Visible = False
                Else
                    tcDec.Visible = True
                    tcDesc2.Visible = True
                    hlDec.NavigateUrl = clC.Fields("decpage")
                    hlDec.Text = clC.Fields("decpage")
                End If
                If clC.Fields("tcpage").Length = 0 Then
                    tcTC.Visible = False
                    tcTC2.Visible = False
                Else
                    tcTC.Visible = True
                    tcTC2.Visible = True
                    hlTC.NavigateUrl = clC.Fields("tcpage")
                    hlTC.Text = clC.Fields("tcpage")
                End If
                GetDealerInfo(clC.Fields("dealerid"))
                GetAgentInfo(clC.Fields("agentsid"))
            End If
    End Sub

    Private Sub GetAgentInfo(xAgentID As Long)
        Dim clA As New clsDBO
        Dim SQL As String
        Dim sTemp As String
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & xAgentID
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            sTemp = clA.Fields("agentno") & vbCrLf
            sTemp = sTemp + clA.Fields("agentname") & vbCrLf
            sTemp = sTemp + clA.Fields("addr1") & vbCrLf
            If clA.Fields("addr2").Length > 0 Then
                sTemp = sTemp + clA.Fields("addr2") & vbCrLf
            End If
            sTemp = sTemp + clA.Fields("city") & " " & clA.Fields("state") & " " & clA.Fields("zip") & vbCrLf
            sTemp = sTemp + clA.Fields("phone")
            txtAgent.Text = sTemp
        End If
    End Sub

    Private Sub GetDealerInfo(xDealerID As Long)
        Dim clD As New clsDBO
        Dim SQL As String
        Dim sTemp As String
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & xDealerID
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            sTemp = clD.Fields("dealerno") & vbCrLf
            sTemp = sTemp + clD.Fields("dealername") & vbCrLf
            sTemp = sTemp + clD.Fields("Addr1") & vbCrLf
            If clD.Fields("Addr2").Length > 0 Then
                sTemp = sTemp + clD.Fields("Addr2") & vbCrLf
            End If
            sTemp = sTemp + clD.Fields("City") & " "
            sTemp = sTemp + clD.Fields("State") & " "
            sTemp = sTemp + clD.Fields("Zip") & vbCrLf
            sTemp = sTemp + clD.Fields("Phone")
            txtDealer.Text = sTemp
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = DateTime.Today
        sEndDate = DateAdd("d", 1, DateTime.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
            ReadOnlyButtons()
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        If hfReadOnly.Value = "True" Then
            btnAddClaim.Enabled = False
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfReadOnly.Value = clSI.Fields("readonly")
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError(xMessage As String)
        hfError.Value = "Visible"
        lblError.Text = xMessage
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub ShowDupeClaim()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwDupeClaim.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub tsContract_TabClick(sender As Object, e As RadTabStripEventArgs) Handles tsContract.TabClick
        If tsContract.SelectedTab.Value = "Breakdown" Then
            pvBreakdown.Selected = True
        End If
        If tsContract.SelectedTab.Value = "Surcharge" Then
            pvSurcharge.Selected = True
        End If
        If tsContract.SelectedTab.Value = "Costs" Then
            pvCost.Selected = True
        End If
        If tsContract.SelectedTab.Value = "Commissions" Then
            pvCommissions.Selected = True
        End If
        If tsContract.SelectedTab.Value = "Payment" Then
            pvPayment.Selected = True
        End If
        If tsContract.SelectedTab.Value = "Document" Then
            pvContractDocument.Selected = True
        End If
        If tsContract.SelectedTab.Value = "Cancellation" Then
            pvCancel.Selected = True
            ucCancel.FillCancellation()
        End If
        If tsContract.SelectedTab.Value = "Transfer" Then
            pvTransfer.Selected = True
        End If
        If tsContract.SelectedTab.Value = "Notes" Then
            pvNotes.Selected = True
        End If
        If tsContract.SelectedTab.Value = "CustModify" Then
            pvCustomerMod.Selected = True
        End If
        If tsContract.SelectedTab.Value = "ContractModify" Then
            pvContractMod.Selected = True
        End If
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAddClaim_Click(sender As Object, e As EventArgs) Handles btnAddClaim.Click
        lblError.Text = ""
        If Not CheckClaimDuplicate() Then
            Dim SQL As String
            Dim clC As New clsDBO
            If hfReadOnly.Value = "True" Then
                Exit Sub
            End If
            pnlContractConfirm.Visible = False
            tsContract.Visible = True
            SQL = "select * from contract "
            SQL = SQL + "where contractid = " & hfContractID.Value
            clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clC.RowCount > 0 Then
                clC.GetRow()
                If clC.Fields("status") = "Paid" Then
                    If clC.Fields("plantypeid") <> 118 And clC.Fields("programid") <> 103 Then
                        GetClaimNo()
                        AddClaimDetail()
                        Response.Redirect("~\claim\claim.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value)
                    Else
                        GetClaimNoGAP()
                        Response.Redirect("~\claim\claimgap.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value)
                    End If
                    Exit Sub
                Else
                    If Not CheckCancelExpire() Then
                        ShowError("Contract is Expired or Cancelled. Can not start claim.")
                        Exit Sub
                    End If
                    tsContract.Visible = False
                    txtContractStatus2.Text = clC.Fields("status")
                    If clC.Fields("plantypeid") <> 118 And clC.Fields("programid") <> 103 Then
                        GetClaimNo()
                        AddClaimDetail()
                    Else
                        GetClaimNoGAP()
                        hfPlanTypeID.Value = clC.Fields("plantypeid")
                        hfProgramID.Value = clC.Fields("programid")
                    End If
                    pnlContractConfirm.Visible = True
                    Exit Sub
                End If
            End If
        End If

    End Sub

    Private Function CheckCancelExpire() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clU As New clsDBO
        CheckCancelExpire = False
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("status").ToLower = "expired" Or clR.Fields("status").ToLower = "pending expired" Or clR.Fields("status").ToLower = "cancelled" Or clR.Fields("status").ToLower = "cancelled before paid" Then
                SQL = "select * from usersecurityinfo "
                SQL = SQL + "where userid = " & hfUserID.Value
                clU.OpenDB(SQL, AppSettings("connstring"))
                If clU.RowCount > 0 Then
                    clU.GetRow()
                    If CBool(clU.Fields("allowcancelexpire")) Then
                        CheckCancelExpire = True
                    End If
                End If
            Else
                CheckCancelExpire = True
            End If
        End If
    End Function

    Private Function CheckClaimDuplicate() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckClaimDuplicate = False
        If Not CheckWithInDays() Then
            If hfContractID.Value.Length > 0 Then
                SQL = "select * from claim "
                SQL = SQL + "where contractid = " & hfContractID.Value & " "
                SQL = SQL + "and credate > '" & DateAdd(DateInterval.Day, -3, DateTime.Today) & "' "
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 0 Then
                    ShowDupeClaim()
                    CheckClaimDuplicate = True
                End If
            End If
        End If
    End Function

    Private Function CheckWithInDays() As Boolean
        CheckWithInDays = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("within3days")) Then
                CheckWithInDays = True
            End If
        End If
    End Function

    Private Sub AddClaimDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lDeductID As Long
        Dim dDeduct As Long
        SQL = "select deductid from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lDeductID = clR.Fields("deductid")
        End If
        SQL = "select * from deductible "
        SQL = SQL + "where deductid = " & lDeductID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dDeduct = clR.Fields("deductamt")
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = 'A01' "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
        Else
            clR.GetRow()
        End If
        clR.Fields("ratetypeid") = 1
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("claimdetailtype") = "Deductible"
        clR.Fields("jobno") = "A01"
        clR.Fields("reqqty") = "1"
        clR.Fields("reqcost") = dDeduct * -1
        clR.Fields("claimdetailstatus") = "Requested"
        clR.Fields("reqamt") = dDeduct * -1
        clR.Fields("creby") = hfUserID.Value
        clR.Fields("credate") = DateTime.Today
        clR.Fields("modby") = hfUserID.Value
        clR.Fields("moddate") = DateTime.Today
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub GetClaimNo()
        Dim SQL As String
        Dim clC As New clsDBO
        Dim sClaimNo As String
        Dim lClaimNo As Long = 0
MoveHere:
        sClaimNo = ""
        SQL = "select max(claimno) as claimno from claim "
        SQL = SQL + "where claimno like 'C1%' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            sClaimNo = clC.Fields("claimno")
            If sClaimNo.Length = 0 Then
                sClaimNo = "C100000000"
            End If
            sClaimNo = sClaimNo.Replace("C1", "")
            lClaimNo = sClaimNo
            lClaimNo = lClaimNo + 1
            sClaimNo = Format(lClaimNo, "100000000")
            sClaimNo = "C" & sClaimNo
        End If

        If sClaimNo.Length > 0 Then
            SQL = "select * from claim "
            SQL = SQL + "where claimno = '" & sClaimNo & "' "
            clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clC.RowCount > 0 Then
                GoTo MoveHere
            Else
                clC.NewRow()
                clC.Fields("claimno") = sClaimNo
                clC.Fields("status") = "Open"
                If hfContractID.Value <> "0" Then
                    clC.Fields("contractid") = hfContractID.Value
                End If
                clC.Fields("lossdate") = DateTime.Today
                clC.Fields("claimactivityid") = 1
                clC.Fields("assignedto") = hfUserID.Value
                clC.Fields("creby") = hfUserID.Value
                clC.Fields("credate") = DateTime.Today
                clC.Fields("moddate") = DateTime.Today
                clC.Fields("modby") = hfUserID.Value
                clC.AddRow()
                clC.SaveDB()
            End If
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimno = '" & sClaimNo & "' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfClaimID.Value = clC.Fields("claimid")
            SQL = "insert into claimopenhistory "
            SQL = SQL + "(claimid, opendate, openby) "
            SQL = SQL + "values (" & hfClaimID.Value & ",'"
            SQL = SQL + DateTime.Today & "',"
            SQL = SQL + hfUserID.Value & ")"
            clC.RunSQL(SQL, AppSettings("connstring"))
        End If
    End Sub

    Private Sub GetClaimNoGAP()
        Dim SQL As String
        Dim clC As New clsDBO
        Dim sClaimNo As String
        Dim lClaimNo As Long = 0
MoveHere:
        sClaimNo = ""
        SQL = "select max(claimno) as claimno from claimgap "
        SQL = SQL + "where claimno like 'G0%' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("claimno").Length > 0 Then
                sClaimNo = clC.Fields("claimno")
                sClaimNo = sClaimNo.Replace("G0", "")
                lClaimNo = sClaimNo
                lClaimNo = lClaimNo + 1
                sClaimNo = Format(lClaimNo, "000000000")
                sClaimNo = "G" & sClaimNo
            Else
                sClaimNo = "G000000000"
            End If
        End If
        If sClaimNo.Length > 0 Then
            SQL = "select * from claimgap "
            SQL = SQL + "where claimno = '" & sClaimNo & "' "
            clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clC.RowCount > 0 Then
                GoTo MoveHere
            Else
                clC.NewRow()
                clC.Fields("claimno") = sClaimNo
                clC.Fields("status") = 1
                If hfContractID.Value <> "0" Then
                    clC.Fields("contractid") = hfContractID.Value
                End If
                clC.Fields("claimdate") = DateTime.Today
                clC.Fields("creby") = hfUserID.Value
                clC.Fields("credate") = DateTime.Today
                clC.Fields("moddate") = DateTime.Today
                clC.Fields("modby") = hfUserID.Value
                clC.AddRow()
                clC.SaveDB()
            End If
        End If
        SQL = "select * from claimgap "
        SQL = SQL + "where claimno = '" & sClaimNo & "' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfClaimID.Value = clC.Fields("claimgapid")
        End If
    End Sub

    Private Sub btnCancelClaim_Click(sender As Object, e As EventArgs) Handles btnCancelClaim.Click
        UpdateContractClaimCancel()
        UpdateClaimNote()
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub UpdateContractClaimCancel()
        Dim clCL As New clsDBO
        Dim SQL As String
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCL.OpenDB(SQL, AppSettings("connstring"))
        If clCL.RowCount > 0 Then
            clCL.GetRow()
            clCL.Fields("status") = "Void"
            clCL.Fields("contractid") = hfContractID.Value
            clCL.SaveDB()
        End If
    End Sub

    Private Sub btnProceedClaim_Click(sender As Object, e As EventArgs) Handles btnProceedClaim.Click
        If hfPlanTypeID.Value <> "118" And hfProgramID.Value <> "103" Then
            UpdateClaimNote()
            pnlContractConfirm.Visible = False
            tsContract.Visible = True
            Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
        Else
            UpdateClaimNote2()
            pnlContractConfirm.Visible = False
            tsContract.Visible = True
            Response.Redirect("~/claim/claimgap.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
        End If

    End Sub

    Private Sub UpdateClaimNote()
        If txtNote.Text.Length = 0 Then
            Exit Sub
        End If
        Dim clCN As New clsDBO
        Dim SQL As String
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnoteid = 0 "
        clCN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        clCN.NewRow()
        clCN.Fields("claimid") = hfClaimID.Value
        clCN.Fields("claimnotetypeid") = 5
        clCN.Fields("note") = txtNote.Text
        clCN.Fields("credate") = Date.Today
        clCN.Fields("creby") = hfUserID.Value
        clCN.AddRow()
        clCN.SaveDB()
    End Sub

    Private Sub UpdateClaimNote2()
        If txtNote.Text.Length = 0 Then
            Exit Sub
        End If
        Dim clCN As New clsDBO
        Dim SQL As String
        SQL = "select * from claimgapnote "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnoteid = 0 "
        clCN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        clCN.NewRow()
        clCN.Fields("claimgapid") = hfClaimID.Value
        clCN.Fields("claimgapnotetypeid") = 3
        clCN.Fields("note") = txtNote.Text
        clCN.Fields("credate") = Date.Today
        clCN.Fields("creby") = hfUserID.Value
        clCN.AddRow()
        clCN.SaveDB()
    End Sub
End Class