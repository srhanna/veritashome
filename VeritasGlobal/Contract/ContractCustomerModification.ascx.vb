﻿Public Class ContractCustomerModification
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        If Not IsPostBack Then
            hfContractID.Value = Request.QueryString("contractid")
            GetServerInfo()
            FillCustomer()
            readonlybuttons
        End If

    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnSave.Visible = False
            End If
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillCustomer()
        Dim clC As New clsDBO
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtAddr1.Text = clC.Fields("addr1")
            txtAddr2.Text = clC.Fields("addr2")
            txtCity.Text = clC.Fields("city")
            txtFName.Text = clC.Fields("fname")
            txtLName.Text = clC.Fields("lname")
            txtPhone.Text = clC.Fields("phone")
            txtZip.Text = clC.Fields("zip")
            cboState.SelectedValue = clC.Fields("state")
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        FillCustomer()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim clC As New clsDBO
        Dim SQL As String
        Dim clMAC As New VeritasGlobalTools.clsMoxyAddressChange

        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            clC.Fields("addr1") = txtAddr1.Text
            clC.Fields("addr2") = txtAddr2.Text
            clC.Fields("city") = txtCity.Text
            clC.Fields("fname") = txtFName.Text
            clC.Fields("lname") = txtLName.Text
            clC.Fields("phone") = txtPhone.Text
            clC.Fields("zip") = txtZip.Text
            clC.Fields("state") = cboState.SelectedValue
            clC.SaveDB()
            clMAC.UpdateMoxy(hfContractID.Value)
        End If
    End Sub
End Class