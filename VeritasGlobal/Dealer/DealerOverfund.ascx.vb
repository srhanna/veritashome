﻿Public Class DealerOverfund
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsRateType.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsRateCategory.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsRateTypePayee.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()

        If Not IsPostBack Then
            pnlAddRateType.Visible = False
            pnlController.Visible = True
            pnlDetail.Visible = False
            pnlSearchRateType.Visible = False
            hfDealerID.Value = Request.QueryString("dealerid")
            hfRateTypePayeeID.Value = 0
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        hfRateTypePayeeID.Value = 0
        FillDetail()
        pnlController.Visible = False
        pnlDetail.Visible = True
    End Sub

    Private Sub btnAddRateType_Click(sender As Object, e As EventArgs) Handles btnAddRateType.Click
        pnlSearchRateType.Visible = False
        pnlAddRateType.Visible = True
        hfRateTypeID.Value = 0
        txtRateType.Text = ""
        cboRateCategoryAdd.ClearSelection()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlController.Visible = True
        pnlDetail.Visible = False
    End Sub

    Private Sub btnCancelCategory_Click(sender As Object, e As EventArgs) Handles btnCancelCategory.Click
        pnlSearchRateType.Visible = True
        pnlAddRateType.Visible = False
    End Sub

    Private Sub btnCancelRateType_Click(sender As Object, e As EventArgs) Handles btnCancelRateType.Click
        pnlDetail.Visible = True
        pnlSearchRateType.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from ratetypepayee "
        SQL = SQL + "where ratetypepayeeid = " & hfRateTypePayeeID.Value & " "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("dealerid") = hfDealerID.Value
        clR.Fields("ratetypeid") = hfRateTypeID.Value
        clR.Fields("payeename") = txtPayeeName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.Text
        clR.Fields("zip") = txtzip.Text
        clR.Fields("phone") = txtPhone.Text
        If clR.RowCount > 0 Then
            clR.Fields("modby") = hfUserID.Value
            clR.Fields("moddate") = Now
        Else
            clR.Fields("credate") = Now
            clR.Fields("creby") = hfUserID.Value
            clR.AddRow()
        End If
        clR.SaveDB()
        rgRateTypePayee.Rebind()
        pnlController.Visible = True
        pnlDetail.Visible = False
    End Sub

    Private Sub btnSaveCategory_Click(sender As Object, e As EventArgs) Handles btnSaveCategory.Click
        Dim SQL As String
        Dim clRT As New clsDBO
        SQL = "select * from ratetype "
        SQL = SQL + "where ratetypename = '" & txtRateType.Text & "' "
        clRT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRT.RowCount = 0 Then
            clRT.NewRow()
        Else
            clRT.GetRow()
        End If
        clRT.Fields("ratetypename") = txtRateTypeAdd.Text
        clRT.Fields("ratecategoryid") = cboRateCategoryAdd.SelectedValue
        If clRT.RowCount = 0 Then
            clRT.AddRow()
        End If
        clRT.SaveDB()
        rgRateType.Rebind()
        pnlSearchRateType.Visible = True
        pnlAddRateType.Visible = False
    End Sub

    Private Sub btnSearchRateType_Click(sender As Object, e As EventArgs) Handles btnSearchRateType.Click
        rgRateType.Rebind()
        pnlDetail.Visible = False
        pnlSearchRateType.Visible = True
    End Sub

    Private Sub rgRateTypePayee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgRateTypePayee.SelectedIndexChanged
        hfRateTypePayeeID.Value = rgRateTypePayee.SelectedValue
        filldetail
        pnlDetail.Visible = True
        pnlController.Visible = False
    End Sub

    Private Sub FillDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from ratetypepayee "
        SQL = SQL + "where ratetypepayeeid = " & hfRateTypePayeeID.Value & " "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtAddr1.Text = clR.Fields("addr1")
            txtAddr2.Text = clR.Fields("addr2")
            txtCity.Text = clR.Fields("city")
            txtCreDate.Text = clR.Fields("credate")
            txtModDate.Text = clR.Fields("moddate")
            txtPayeeName.Text = clR.Fields("payeename")
            cboState.Text = clR.Fields("state")
            txtZip.Text = clR.Fields("zip")
            txtPhone.Text = clR.Fields("phone")
            txtCreBy.Text = GetUserInfo(clR.Fields("creby"))
            If clR.Fields("modby").Length > 0 Then
                txtModBy.Text = GetUserInfo(clR.Fields("modby"))
            End If
        Else
            txtAddr1.Text = ""
            txtAddr2.Text = ""
            txtCity.Text = ""
            txtCreDate.Text = ""
            txtModDate.Text = ""
            txtPayeeName.Text = ""
            cboState.Text = ""
            txtZip.Text = ""
            txtPhone.Text = ""
            txtCreBy.Text = ""
            txtModDate.Text = ""
        End If
    End Sub

    Private Sub rgRateType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgRateType.SelectedIndexChanged
        hfRateTypeID.Value = rgRateType.SelectedValue
        txtRateType.Text = GetRateType(hfRateTypeID.Value)
        pnlDetail.Visible = True
        pnlSearchRateType.Visible = False
    End Sub

    Public Function GetUserInfo(xUserID As Long) As String
        GetUserInfo = ""
        Dim SQL As String
        Dim clU As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & xUserID
        clU.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
            GetUserInfo = clU.Fields("fname") & clU.Fields("lname")
        End If
    End Function
End Class