﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI
Imports System.IO

Public Class DealerDocuments
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfDealerID.Value = Request.QueryString("dealerid")
        If Not IsPostBack Then
            GetServerInfo()
            ReadOnlyButtons()
            pnlList.Visible = True
            pnlAdd.Visible = False
            pnlDetail.Visible = False
            FillGrid()
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnSave.Enabled = False
                btnAdd.Enabled = False
                btnDelete.Enabled = False
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillGrid()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select dealerdocumentid, documentname, documentdesc, documentlink "
        SQL = SQL + "from dealerdocument "
        SQL = SQL + "where dealerid = " & hfDealerID.Value & " "
        SQL = SQL + "and deleted = 0 "
        rgContractDocument.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgContractDocument.Rebind()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlAdd.Visible = True
        pnlList.Visible = False
    End Sub

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim sDocLink As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetDealerNo()
        Dim folderPath As String = Server.MapPath("~") & "\documents\dealers\" & hfDealerNo.Value & "_" & FileUpload2.FileName
        sDocLink = "~/documents/dealers/" & hfDealerNo.Value & "_" & FileUpload2.FileName
        FileUpload2.SaveAs(folderPath)
        SQL = "select * from dealerdocument "
        SQL = SQL + "where dealerid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        clR.Fields("dealerid") = hfDealerID.Value
        clR.Fields("documentname") = txtDocName.Text
        clR.Fields("documentdesc") = txtDocDesc.Text
        clR.Fields("documentlink") = sDocLink
        clR.Fields("creby") = hfUserID.Value
        clR.Fields("credate") = Now
        clR.Fields("moddate") = Now
        clR.Fields("modby") = hfUserID.Value
        clR.AddRow()
        clR.SaveDB()
        pnlAdd.Visible = False
        pnlList.Visible = True
        FillGrid()
    End Sub

    Private Sub GetDealerNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfDealerNo.Value = clR.Fields("dealerno")
        End If
    End Sub

    Private Sub rgContractDocument_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgContractDocument.SelectedIndexChanged
        pnlList.Visible = False
        pnlDetail.Visible = True
        hfDocID.Value = rgContractDocument.SelectedValue
        FillDetail()
    End Sub

    Private Sub FillDetail()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from dealerdocument "
        SQL = SQL + "where dealerdocumentid = " & hfDocID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtCreBy.Text = GetUserInfo(clR.Fields("creby"))
            txtCreDate.Text = clR.Fields("credate")
            txtDescDetail.Text = clR.Fields("documentdesc")
            txtModBy.Text = GetUserInfo(clR.Fields("modby"))
            txtTitleDetail.Text = clR.Fields("documentname")
            txtModDate.Text = clR.Fields("moddate")
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update dealerdocument "
        SQL = SQL + "set deleted = 1 "
        SQL = SQL + "where dealerdocumentid = " & hfDocID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * dealerdocument "
        SQL = SQL + "where dealerdocumentid = " & hfDocID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("documentname") = txtTitleDetail.Text
            clR.Fields("documentdesc") = txtDescDetail.Text
            clR.Fields("modby") = hfUserID.Value
            clR.Fields("moddate") = Now
            clR.SaveDB()
        End If
    End Sub

    Private Sub btnCloseAdd_Click(sender As Object, e As EventArgs) Handles btnCloseAdd.Click
        pnlAdd.Visible = False
        pnlList.Visible = True
    End Sub
End Class