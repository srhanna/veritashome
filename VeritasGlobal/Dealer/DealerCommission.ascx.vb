﻿Imports Telerik.Web.UI

Public Class DealerCommission
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsRateType.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsRateCategory.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsPayee.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")

        If Not IsPostBack Then
            GetServerInfo()
            hfDealerID.Value = Request.QueryString("dealerid")
            pnlCommission.Visible = True
            pnlCommissionDetail.Visible = False
            pnlSearchPayee.Visible = False
            pnlSearchRateType.Visible = False
            pnlAddRateType.Visible = False
            pnlConfirmDelete.Visible = False
            pnlAddPayee.Visible = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        hfDealerID.Value = Request.QueryString("dealerid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnAddCommission_Click(sender As Object, e As EventArgs) Handles btnAddCommission.Click
        pnlCommission.Visible = False
        pnlCommissionDetail.Visible = True
        ClearCommissionDetail()
    End Sub

    Private Sub ClearCommissionDetail()
        txtRateType.Text = ""
        rdpStartDate.Clear()
        txtPayee.Text = ""
        txtPercent.Text = ""
        txtAmount.Text = ""
        rdpEndDate.Clear()
        hfCommissionID.Value = "0"
        hfPayeeID.Value = "0"
    End Sub

    Private Sub rgDealerCommission_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgDealerCommission.SelectedIndexChanged
        Dim SQL As String
        Dim clDC As New clsDBO
        SQL = "select * from commissions "
        SQL = SQL + "where commissionid = " & rgDealerCommission.SelectedValue
        clDC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clDC.RowCount > 0 Then
            clDC.GetRow()
            hfCommissionID.Value = clDC.Fields("commissionid")
            hfRateTypeID.Value = clDC.Fields("ratetypeid")
            txtRateType.Text = GetRateType(clDC.Fields("ratetypeid"))
            rdpStartDate.SelectedDate = clDC.Fields("startdate")
            txtPayee.Text = GetPayee(clDC.Fields("Payeeid"))
            txtAmount.Text = clDC.Fields("amt")
            hfPayeeID.Value = clDC.Fields("payeeid")
            If IsDate(clDC.Fields("enddate")) Then
                rdpEndDate.SelectedDate = clDC.Fields("enddate")
            End If
            lblCreBy.Text = GetUserInfo(clDC.Fields("creby"))
            lblCreDate.Text = clDC.Fields("credate")
            lblModBy.Text = GetUserInfo(clDC.Fields("modby"))
            lblModDate.Text = clDC.Fields("moddate")
            txtRateType.Focus()
            pnlCommission.Visible = False
            pnlCommissionDetail.Visible = True
        End If
    End Sub

    Private Sub btnCancelCommission_Click(sender As Object, e As EventArgs) Handles btnCancelCommission.Click
        pnlCommissionDetail.Visible = False
        pnlCommission.Visible = True
    End Sub

    Private Sub btnSaveCommission_Click(sender As Object, e As EventArgs) Handles btnSaveCommission.Click
        Dim bAdd As Boolean
        Dim SQL As String
        Dim clCH As New clsDBO
        Dim bChange As Boolean
        SQL = "select * from commissions "
        SQL = SQL + "where commissionid = " & hfCommissionID.Value
        clCH.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCH.RowCount = 0 Then
            clCH.NewRow()
        Else
            clCH.GetRow()
        End If
        If clCH.Fields("ratetypeid") <> hfRateTypeID.Value Then
            bChange = True
        End If
        clCH.Fields("ratetypeid") = hfRateTypeID.Value
        clCH.Fields("dealerid") = hfDealerID.Value
        If clCH.Fields("startdate").Length > 0 Then
            If clCH.Fields("startdate") <> rdpStartDate.SelectedDate Then
                bChange = True
            End If
        End If
        clCH.Fields("startdate") = rdpStartDate.SelectedDate
        If clCH.Fields("payeeid") <> hfPayeeID.Value Then
            bChange = True
        End If
        clCH.Fields("payeeid") = hfPayeeID.Value
        clCH.Fields("programid") = 1
        If clCH.Fields("amt") <> txtAmount.Text Then
            bChange = True
        End If
        clCH.Fields("amt") = txtAmount.Text
        clCH.Fields("peramt") = txtPercent.Text
        If IsDate(rdpEndDate.SelectedDate) Then
            If clCH.Fields("enddate").Length > 0 Then
                If clCH.Fields("enddate") <> rdpEndDate.SelectedDate Then
                    bChange = True
                End If
            Else
                bChange = True
            End If
            clCH.Fields("enddate") = rdpEndDate.SelectedDate
        End If

        If clCH.RowCount = 0 Then
            clCH.Fields("creby") = hfUserID.Value
            clCH.Fields("credate") = Today
            clCH.Fields("moddate") = Today
            clCH.Fields("modby") = hfUserID.Value
            clCH.AddRow()
            bAdd = True
        Else
            clCH.Fields("moddate") = Today
            clCH.Fields("modby") = hfUserID.Value
        End If
        clCH.SaveDB()
        If bAdd Then
            GetCommissionID()
        End If
        If bAdd Then
            AddCommissions()
        Else
            If bChange Then
                DeleteCommissionDetail()
                AddCommissions()
            End If
        End If
        rgDealerCommission.Rebind()
        pnlCommissionDetail.Visible = False
        pnlCommission.Visible = True

    End Sub

    Private Sub DeleteCommissionDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "delete commissions "
        SQL = SQL + "where commissionid = " & hfCommissionID.Value
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Sub AddCommissions()
        rgDealerCommission.Rebind()
    End Sub

    Private Sub GetCommissionID()
        Dim SQL As String
        Dim clCH As New clsDBO
        SQL = "select max(commissionid) as CH from commissions "
        SQL = SQL + "where creby = " & hfUserID.Value
        clCH.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCH.RowCount > 0 Then
            clCH.GetRow()
            hfCommissionID.Value = clCH.Fields("ch")
        End If
    End Sub

    Private Sub DeleteCommission()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "delete commissions "
        SQL = SQL + "where commissionid = " & hfCommissionID.Value
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgDealerCommission.Rebind()
    End Sub

    Private Sub btnSearchRateType_Click(sender As Object, e As EventArgs) Handles btnSearchRateType.Click
        rgRateType.Rebind()
        pnlCommissionDetail.Visible = False
        pnlSearchRateType.Visible = True
    End Sub

    Private Sub rgRateType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgRateType.SelectedIndexChanged
        hfRateTypeID.Value = rgRateType.SelectedValue
        txtRateType.Text = GetRateType(hfRateTypeID.Value)
        pnlCommissionDetail.Visible = True
        pnlSearchRateType.Visible = False
    End Sub

    Private Sub btnPayee_Click(sender As Object, e As EventArgs) Handles btnPayee.Click
        rgPayee.Rebind()
        pnlSearchPayee.Visible = True
        pnlCommissionDetail.Visible = False
    End Sub

    Private Sub rgPayee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgPayee.SelectedIndexChanged
        hfPayeeID.Value = rgPayee.SelectedValue
        txtPayee.Text = GetPayee(rgPayee.SelectedValue)
        pnlSearchPayee.Visible = False
        pnlCommissionDetail.Visible = True
    End Sub

    Private Sub btnAddRateType_Click(sender As Object, e As EventArgs) Handles btnAddRateType.Click
        pnlSearchRateType.Visible = False
        pnlAddRateType.Visible = True
    End Sub

    Private Sub btnSaveCategory_Click(sender As Object, e As EventArgs) Handles btnSaveCategory.Click
        Dim SQL As String
        Dim clRT As New clsDBO
        SQL = "select * from ratetype "
        SQL = SQL + "where ratetypename = '" & txtRateType.Text & "' "
        clRT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRT.RowCount = 0 Then
            clRT.NewRow()
        Else
            clRT.GetRow()
        End If
        clRT.Fields("ratetypename") = txtRateTypeAdd.Text
        clRT.Fields("ratecategoryid") = cboRateCategoryAdd.SelectedValue
        If clRT.RowCount = 0 Then
            clRT.AddRow()
        End If
        clRT.SaveDB()
        rgRateType.Rebind()
        pnlSearchRateType.Visible = True
        pnlAddRateType.Visible = False
    End Sub

    Private Sub btnCancelCategory_Click(sender As Object, e As EventArgs) Handles btnCancelCategory.Click
        pnlSearchRateType.Visible = True
        pnlAddRateType.Visible = False
    End Sub

    Private Sub btnClearPayeeInfo_Click(sender As Object, e As EventArgs) Handles btnClearPayeeInfo.Click
        hfPayeeID.Value = 0
        txtPayee.Text = ""
        pnlSearchPayee.Visible = False
        pnlCommissionDetail.Visible = True
    End Sub

    Private Sub btnDeleteCommission_Click(sender As Object, e As EventArgs) Handles btnDeleteCommission.Click
        pnlCommissionDetail.Visible = False
        pnlConfirmDelete.Visible = True
        lblProgramConfirm.Text = txtPayee.Text
        lblRateTypeConfirm.Text = txtRateType.Text
        lblAmtConfirm.Text = txtAmount.Text
    End Sub

    Private Sub btnNoConfirm_Click(sender As Object, e As EventArgs) Handles btnNoConfirm.Click
        pnlCommissionDetail.Visible = True
        pnlConfirmDelete.Visible = False
    End Sub

    Private Sub btnYesConfirm_Click(sender As Object, e As EventArgs) Handles btnYesConfirm.Click
        DeleteCommission()
        rgDealerCommission.Rebind()
        pnlCommission.Visible = True
        pnlConfirmDelete.Visible = False
    End Sub

    Private Sub btnCancelRateType_Click(sender As Object, e As EventArgs) Handles btnCancelRateType.Click
        pnlCommissionDetail.Visible = True
        pnlSearchRateType.Visible = False
    End Sub

    Private Sub btnAddPayee_Click(sender As Object, e As EventArgs) Handles btnAddPayee.Click
        cboState.ClearSelection()
        txtCompanyName.Text = ""
        txtAddr1.Text = ""
        txtAddr2.Text = ""
        txtCity.Text = ""
        txtZip.Text = ""
        txtPhone.Text = ""
        pnlAddPayee.Visible = True
        pnlSearchPayee.Visible = False
    End Sub

    Private Sub btnAddPayeeCancel_Click(sender As Object, e As EventArgs) Handles btnAddPayeeCancel.Click
        pnlAddPayee.Visible = False
        pnlSearchPayee.Visible = True
    End Sub

    Private Sub btnAddPayeeSave_Click(sender As Object, e As EventArgs) Handles btnAddPayeeSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from payee "
        SQL = SQL + "where payeeid = 0 "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("companyname") = txtCompanyName.Text
            clR.Fields("addr1") = txtAddr1.Text
            clR.Fields("addr2") = txtAddr2.Text
            clR.Fields("city") = txtCity.Text
            clR.Fields("state") = cboState.SelectedValue
            clR.Fields("zip") = txtZip.Text
            clR.Fields("phone") = txtPhone.Text
            clR.AddRow()
            clR.SaveDB()
        End If
        rgPayee.Rebind()
        pnlAddPayee.Visible = False
        pnlSearchPayee.Visible = True
    End Sub


End Class