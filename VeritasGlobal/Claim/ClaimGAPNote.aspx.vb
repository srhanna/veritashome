﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class ClaimGAPNote
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsNote.ConnectionString = AppSettings("connstring")
        dsNoteType.ConnectionString = AppSettings("connstring")
        hfClaimID.Value = Request.QueryString("claimid")
        If Not IsPostBack Then
            GetServerInfo()
            pnlDetail.Visible = False
            pnlList.Visible = True
            rgNote.Rebind()
        End If


    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnAddNote_Click(sender As Object, e As EventArgs) Handles btnAddNote.Click
        pnlDetail.Visible = True
        pnlList.Visible = False
        txtNote.Content = ""
        txtCreBy.Text = ""
        txtCreDate.Text = ""
        hfClaimNoteID.Value = 0
    End Sub

    Private Sub rgNote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgNote.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        pnlDetail.Visible = True
        pnlList.Visible = False
        hfClaimNoteID.Value = rgNote.SelectedValue
        SQL = "select * from claimgapnote cn "
        SQL = SQL + "inner join userinfo ui on ui.userid = cn.creby "
        SQL = SQL + "where claimnoteid = " & hfClaimNoteID.Value & " "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            cboNoteType.SelectedValue = clR.Fields("claimgapnotetypeid")
            txtNote.Content = clR.Fields("note")
            txtCreBy.Text = clR.Fields("username")
            txtCreDate.Text = clR.Fields("credate")
            btnSave.Visible = True
            If hfUserID.Value <> clR.Fields("creby") Then
                btnSave.Visible = False
            End If
            If DateDiff(DateInterval.Hour, CDate(txtCreDate.Text), Now) > 24 Then
                btnSave.Visible = False
            End If
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        If hfClaimNoteID.Value.Length = 0 Then
            hfClaimNoteID.Value = 0
        End If
        SQL = "select * from claimgapnote cn "
        SQL = SQL + "where claimnoteid = " & hfClaimNoteID.Value & " "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("claimgapid") = hfClaimID.Value
        clR.Fields("claimgapnotetypeid") = cboNoteType.SelectedValue
        Dim sTemp As String
        sTemp = txtNote.Text
        clR.Fields("note") = txtNote.Content
        clR.Fields("notetext") = sTemp
        clR.Fields("moddate") = Now
        clR.Fields("modby") = hfUserID.Value
        If clR.RowCount = 0 Then
            clR.Fields("credate") = Now
            clR.Fields("creby") = hfUserID.Value
            clR.AddRow()
        End If
        clR.SaveDB()
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgNote.Rebind()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgNote.SelectedIndexes.Clear()
    End Sub
End Class