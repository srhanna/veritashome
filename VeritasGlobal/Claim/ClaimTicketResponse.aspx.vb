﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ClaimTicketResponse
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsCDT.ConnectionString = AppSettings("connstring")
        dsClaimDetailStatus.ConnectionString = AppSettings("connstring")
        dsClaimReason.ConnectionString = AppSettings("connstring")
        dsJobs.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            pnlDetail.Visible = False
            pnlList.Visible = True
            FillClaimCombo()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
            FillClaimCombo()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                btnInspectionWex.Enabled = False
                btnCarfaxPayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub FillClaimCombo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimid, claimno from claim "
        SQL = SQL + "where claimid in (select claimid from veritasclaimticket.dbo.ticket where responseid <> 1 and statusid = 2) "
        cboClaim.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        cboClaim.DataBind()
        hfClaimID.Value = cboClaim.SelectedValue
        filllist
    End Sub

    Private Sub FillList()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select cd.claimdetailid, claimdetailtype,cd.partno, cd.claimdesc, claimdetailstatus, jobno, cd.losscode, lc.losscodedesc,
                payeename, reqamt, authamt, taxamt, paidamt, totalamt, r.response From claimdetail cd 
                inner join veritasclaimticket.dbo.ticket T on t.claimdetailid = cd.claimdetailid
                inner join veritasclaimticket.dbo.response R on t.responseid = r.responseid 
                left join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid
                Left join claimlosscode lc on lc.losscode = cd.losscode "
        SQL = SQL + "where t.claimid = " & cboClaim.SelectedValue & " "
        SQL = SQL + "and (t.responseid = 2 or t.responseid = 3) and statusid = 2 "
        SQL = SQL + "order by jobno "
        rgJobs.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgJobs.Rebind()

    End Sub

    Private Sub cboClaim_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClaim.SelectedIndexChanged
        hfClaimID.Value = cboClaim.SelectedValue
    End Sub

    Private Sub rgJobs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgJobs.SelectedIndexChanged
        hfClaimDetailID.Value = rgJobs.SelectedValue
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clT As New clsDBO
        SQL = "select * from claimdetail where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select * from veritasclaimticket.dbo.ticket t "
            SQL = SQL + "inner join veritasclaimticket.dbo.response r on r.responseid = t.responseid "
            SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value & " "
            clT.OpenDB(SQL, AppSettings("connstring"))
            If clT.RowCount > 0 Then
                clT.GetRow()
                cboClaimDetailType.SelectedValue = clR.Fields("claimdetailtype")
                txtJobNo.Text = clR.Fields("jobno")
                cboClaimDetailStatus.SelectedValue = clR.Fields("claimdetailstatus")
                txtPartNo.Text = clR.Fields("partno")
                txtClaimDesc.Text = clR.Fields("claimdesc")
                txtLossCode.Text = clR.Fields("losscode")
                cboReason.SelectedValue = clR.Fields("claimreasonid")
                txtLossCodeDesc.Text = GetLossCode(clR.Fields("losscode"))
                CalcPayee(clR.Fields("claimpayeeid"))
                txtReqQty.Text = clR.Fields("reqqty")
                txtReqCost.Text = clR.Fields("reqcost")
                txtReqAmt.Text = clR.Fields("reqamt")
                txtAuthQty.Text = clR.Fields("authqty")
                txtAuthAmt.Text = clR.Fields("authcost")
                txtTaxRate.Text = clR.Fields("taxper")
                txtTaxAmt.Text = clR.Fields("taxamt")
                txtTotalAmt.Text = clR.Fields("totalamt")
                txtAuthAmt.Text = clR.Fields("authamt")
                If clR.Fields("dateauth").Length > 0 Then
                    rdpAuthorizedDate.SelectedDate = clR.Fields("dateauth")
                Else
                    rdpAuthorizedDate.Clear()
                End If
                If clR.Fields("dateapprove").Length > 0 Then
                    rdpDateApprove.SelectedDate = clR.Fields("dateapprove")
                Else
                    rdpDateApprove.Clear()
                End If
                txtResponse.Text = clT.Fields("response")
                cboRateType.SelectedValue = clR.Fields("ratetypeid")
                If clT.Fields("responseid") = 2 Then
                    btnCloseTicket.Visible = True
                    btnCancelClaimDetail.Visible = True
                    btnSaveClaimDetail.Visible = False
                Else
                    btnCloseTicket.Visible = False
                    btnCancelClaimDetail.Visible = True
                    btnSaveClaimDetail.Visible = True
                End If
                pnlList.Visible = False
                pnlDetail.Visible = True
            End If
        End If
    End Sub

    Private Sub GetResponse(xResponseID As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from response "
        SQL = SQL + "where responseid = " & xResponseID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtResponse.Text = clR.Fields("response")
        End If
    End Sub

    Private Sub CalcPayee(xClaimPayeeID As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & xClaimPayeeID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtPayeeNo.Text = clR.Fields("payeeno")
            txtPayeeName.Text = clR.Fields("payeename")
        End If
    End Sub

    Private Function GetLossCode(xLossCode As String) As String
        GetLossCode = ""
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimlosscode "
        SQL = SQL + "where losscode = '" & xLossCode & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetLossCode = clR.Fields("losscodedesc")
        End If

    End Function

    Private Sub txtAuthQty_TextChanged(sender As Object, e As EventArgs) Handles txtAuthQty.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        If txtAuthQty.Text.Length > 0 Then
            dQty = txtAuthQty.Text
        Else
            dQty = 0
        End If
        If txtAuthCost.Text.Length > 0 Then
            dCost = txtAuthCost.Text
        Else
            dCost = 0
        End If
        If txtTaxRate.Text.Length > 0 Then
            dTaxRate = txtTaxRate.Text / 100
        Else
            dTaxRate = 0
        End If
        dTax = (dQty * dCost) * dTaxRate
        txtTaxAmt.Text = dTax
        txtAuthAmt.Text = (dQty * dCost)
        txtTotalAmt.Text = (dQty * dCost) + dTax
        txtAuthCost.Focus()
    End Sub

    Private Sub txtAuthCost_TextChanged(sender As Object, e As EventArgs) Handles txtAuthCost.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        If txtAuthQty.Text.Length > 0 Then
            dQty = txtAuthQty.Text
        Else
            dQty = 0
        End If
        If txtAuthCost.Text.Length > 0 Then
            dCost = txtAuthCost.Text
        Else
            dCost = 0
        End If
        If txtTaxRate.Text.Length > 0 Then
            dTaxRate = CDbl(txtTaxRate.Text) / 100
        Else
            dTaxRate = 0
        End If
        dTax = (dQty * dCost) * dTaxRate
        dTax = (Decimal.Round(dTax * 100) / 100)
        txtTaxAmt.Text = dTax
        txtAuthAmt.Text = (dQty * dCost)
        txtTotalAmt.Text = (dQty * dCost) + dTax
        txtTaxRate.Focus()
    End Sub

    Private Sub txtTaxRate_TextChanged(sender As Object, e As EventArgs) Handles txtTaxRate.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        If txtAuthQty.Text.Length > 0 Then
            dQty = txtAuthQty.Text
        Else
            dQty = 0
        End If
        If txtAuthCost.Text.Length > 0 Then
            dCost = txtAuthCost.Text
        Else
            dCost = 0
        End If
        If txtTaxRate.Text.Length > 0 Then
            dTaxRate = CDbl(txtTaxRate.Text) / 100
        Else
            dTaxRate = 0
        End If
        dTax = (dQty * dCost) * dTaxRate
        dTax = (Decimal.Round(dTax * 100) / 100)
        txtTaxAmt.Text = dTax
        txtAuthAmt.Text = (dQty * dCost)
        txtTotalAmt.Text = (dCost * dQty) + dTax
        txtTotalAmt.Focus()
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimTeamOpen_Click(sender As Object, e As EventArgs) Handles btnClaimTeamOpen.Click
        Response.Redirect("~/claim/claimteamopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRFClaimSubmit_Click(sender As Object, e As EventArgs) Handles btnRFClaimSubmit.Click
        Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimAudit_Click(sender As Object, e As EventArgs) Handles btnClaimAudit.Click
        Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketMessage_Click(sender As Object, e As EventArgs) Handles btnTicketMessage.Click
        Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketResponse_Click(sender As Object, e As EventArgs) Handles btnTicketResponse.Click
        Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click
        Response.Redirect("~/claim/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub FillRateType()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select ratetypeid, RateTypeName from ratetype "
        SQL = SQL + "where ratetypeid in (1,2834, 1101) "

        SQL = SQL + "or ratetypeid in (select RateTypeID from ratetype "
        SQL = SQL + "where ratecategoryid = 5 "
        SQL = SQL + "and ratetypeid in (select ratetypeid "
        SQL = SQL + "from contractcommissions "
        SQL = SQL + "where agentid in (select agentsid from contract "
        SQL = SQL + "where contractid in (select contractid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + ")))) "
        SQL = SQL + "or RateTypeID in (select 10 from contract "
        SQL = SQL + "where contractid in (select contractid from claim where claimid = " & hfClaimID.Value & " ) "
        SQL = SQL + "and (contractno like 'r%' or contractno like 'vel%')) "
        SQL = SQL + "or RateTypeID in (select 2868 from contract "
        SQL = SQL + "where contractid in (select contractid from claim where claimid = " & hfClaimID.Value & " ) "
        SQL = SQL + "and (contractno like 'va%' or contractno like 'vg%' or contractno like 'vep%')) "
        SQL = SQL + "or RateTypeID in (select 24 from contract c "
        SQL = SQL + "inner join contractamt ca on c.contractid = ca.contractid "
        SQL = SQL + "where c.contractid in (select contractid from claim where claimid =  " & hfClaimID.Value & " ) "
        SQL = SQL + "and ca.RateTypeID = 24) "
        SQL = SQL + "or RateTypeID in (select 25 from contract c "
        SQL = SQL + "inner join contractamt ca on c.contractid = ca.contractid "
        SQL = SQL + "where c.contractid in (select contractid from claim where claimid = " & hfClaimID.Value & " ) "
        SQL = SQL + "and ca.RateTypeID = 25) "
        SQL = SQL + "order by ratetypeid "
        cboRateType.Items.Clear()
        Dim a As Long
        a = cboRateType.Items.Count
        Dim i1 As New Telerik.Web.UI.RadComboBoxItem
        i1.Value = 0
        i1.Text = ""
        cboRateType.Items.Add(i1)
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                i1 = New Telerik.Web.UI.RadComboBoxItem
                i1.Value = clR.Fields("ratetypeid")
                i1.Text = clR.Fields("ratetypename")
                cboRateType.Items.Add(i1)
            Next
        End If
    End Sub

    Private Sub btnSaveClaimDetail_Click(sender As Object, e As EventArgs) Handles btnSaveClaimDetail.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimdesc") = txtClaimDesc.Text
            clR.Fields("claimdetailtype") = cboClaimDetailType.SelectedValue
            If txtJobNo.Text.Length > 0 Then
                clR.Fields("jobno") = txtJobNo.Text
            End If
            clR.Fields("losscode") = txtLossCode.Text
            clR.Fields("reqamt") = txtReqAmt.Text
            clR.Fields("authcost") = txtAuthCost.Text
            If txtTaxRate.Text.Length > 0 Then
                clR.Fields("taxper") = CDbl(txtTaxRate.Text) / 100
            End If
            If txtTaxAmt.Text.Length = 0 Then
                clR.Fields("taxamt") = 0
            Else
                If clR.Fields("authqty").Length = 0 Then
                    clR.Fields("authqty") = 0
                End If
                clR.Fields("taxamt") = txtTaxAmt.Text
            End If

            clR.Fields("totalamt") = txtTotalAmt.Text
            clR.Fields("Authamt") = txtAuthAmt.Text
            clR.Fields("claimdetailstatus") = cboClaimDetailStatus.SelectedValue
            clR.Fields("claimreasonid") = cboReason.SelectedValue
            If Not rdpAuthorizedDate.SelectedDate Is Nothing Then
                clR.Fields("dateauth") = rdpAuthorizedDate.SelectedDate
            Else
                clR.Fields("dateauth") = ""
            End If
            If Not rdpDateApprove.SelectedDate Is Nothing Then
                clR.Fields("dateapprove") = rdpDateApprove.SelectedDate
            Else
                clR.Fields("dateapprove") = ""
            End If
            If clR.Fields("ratetypeid") = "1" Then
                If cboRateType.SelectedValue <> "1" Then
                    clR.Fields("slushbyid") = hfUserID.Value
                    clR.Fields("slushdate") = Today
                End If
            End If
            clR.Fields("ratetypeid") = cboRateType.SelectedValue
            clR.Fields("reqqty") = txtReqQty.Text
            clR.Fields("reqcost") = txtReqCost.Text
            clR.Fields("authqty") = txtAuthQty.Text
            clR.Fields("authcost") = txtAuthCost.Text
            clR.Fields("moddate") = DateTime.Today
            clR.Fields("modby") = hfUserID.Value
            'If cboClaimDetailStatus.SelectedValue = "Denied" Then
            '    CheckClose()
            '    GoTo MoveToSave
            'End If
            'If cboClaimDetailStatus.SelectedValue = "Cancelled" Then
            '    CheckClose()
            '    GoTo MoveToSave
            'End If
            'If cboClaimDetailStatus.SelectedValue = "Requested" Then
            '    CheckClose()
            '    GoTo MoveToSave
            'End If
            If clR.Fields("claimdetailstatus") <> "Paid" Then
                If cboClaimDetailStatus.SelectedValue = "Approved" Then
                    If txtAuthAmt.Text.Length = 0 Then
                        txtAuthAmt.Text = 0
                    End If
                    If clR.Fields("claimdetailid").Length > 0 Then
                        If Not CheckLimitApproved(txtAuthAmt.Text, clR.Fields("claimdetailid")) Then
                            lblApprovedError.Text = "Send to Manager for Approval"
                            GoTo MoveHere
                        End If
                    Else
                        If Not CheckLimitApproved(txtAuthAmt.Text, 0) Then
                            lblApprovedError.Text = "Send to Manager for Approval"
                            GoTo MoveHere
                        End If
                    End If
                End If
                If cboClaimDetailStatus.SelectedValue = "Authorized" Then
                    If txtAuthAmt.Text.Length = 0 Then
                        txtAuthAmt.Text = 0
                    End If
                    If Not CheckLimitAuthorized(txtAuthAmt.Text, clR.Fields("claimdetailid")) Then
                        lblApprovedError.Text = "Send to Manager for Approval"
                        clR.Fields("claimdetailstatus") = "Requested"
                    End If
                End If

            End If
MoveToSave:
            If clR.RowCount = 0 Then
                clR.Fields("credate") = DateTime.Today
                clR.Fields("creby") = hfUserID.Value
                clR.AddRow()
            End If
            clR.SaveDB()
MoveHere:
            pnlDetail.Visible = False
            pnlList.Visible = True
            FillList()
            rgJobs.Rebind()
            SQL = "update claim "
            SQL = SQL + "set moddate = '" & DateTime.Today & "', "
            SQL = SQL + "modby = " & hfUserID.Value & " "
            SQL = SQL + "where claimid = " & hfClaimID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If
        CloseTicket()
    End Sub

    'Private Sub CheckClose()
    '    Dim clR As New clsDBO
    '    Dim SQL As String
    '    SQL = "select claimdetailid from claimdetail "
    '    SQL = SQL + "where claimid = " & hfClaimID.Value & " "
    '    SQL = SQL + "and claimdetailstatus = 'Requested' "
    '    clR.OpenDB(SQL, AppSettings("connstring"))
    '    If clR.RowCount = 0 Then
    '        CloseClaim()
    '        If cboClaimDetailStatus.SelectedValue = "Authorized" Then
    '            UpdateAuthorizedDate()
    '        End If
    '        If cboClaimDetailStatus.SelectedValue = "Approved" Then
    '            UpdateApproveDate()
    '        End If
    '    End If
    'End Sub

    Private Function CheckLimitApproved(xAmt As Double, xClaimDetailID As Long) As Boolean
        CheckLimitApproved = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dLimit As Double
        SQL = "select claimapprove from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dLimit = clR.Fields("claimapprove")
        Else
            dLimit = 0
        End If
        SQL = "select sum(authamt) as Amt from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'Authorized') "
        SQL = SQL + "and claimdetailid <> " & xClaimDetailID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If dLimit > CDbl(clR.Fields("amt")) + xAmt Then
                    CheckLimitApproved = True
                End If
            Else
                If dLimit > xAmt Then
                    CheckLimitApproved = True
                End If
            End If
        End If
    End Function

    Private Function CheckLimitAuthorized(xAmt As Double, xClaimDetailID As Long) As Boolean
        CheckLimitAuthorized = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dLimit As Double
        SQL = "select claimauth from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dLimit = clR.Fields("claimauth")
        Else
            dLimit = 0
        End If
        SQL = "select sum(authamt) as Amt from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'Authorized') "
        SQL = SQL + "and claimdetailid <> " & xClaimDetailID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If dLimit > CDbl(clR.Fields("amt")) + xAmt Then
                    CheckLimitAuthorized = True
                End If
            Else
                If dLimit > xAmt Then
                    CheckLimitAuthorized = True
                End If
            End If
        End If
    End Function

    Private Sub btnCancelClaimDetail_Click(sender As Object, e As EventArgs) Handles btnCancelClaimDetail.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
        FillList()
    End Sub

    Private Sub btnCloseTicket_Click(sender As Object, e As EventArgs) Handles btnCloseTicket.Click
        CloseTicket()
        FillList()
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub CloseTicket()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update veritasclaimticket.dbo.ticket "
        SQL = SQL + "set statusid = 4 "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub
End Class