﻿Imports System.Configuration.ConfigurationManager
Imports System.Data.SqlClient
Imports AjaxControlToolkit
Imports AjaxControlToolkit.HTMLEditor.ToolbarButton
Imports DocumentFormat.OpenXml.Office2013.Excel
Imports Telerik.Web.UI
Imports Telerik.Windows.Documents.Spreadsheet.Expressions.Functions


Public Class ClaimPart1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        dsStates.ConnectionString = AppSettings("connstring")
        dsClaimPayee.ConnectionString = AppSettings("connstring")
        dsLossCode.ConnectionString = AppSettings("connstring")
        dsParts.ConnectionString = AppSettings("connstring")
        dsLabor.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            pnlPartList.Visible = True
            pnlPartDetail.Visible = False
            pnlAddClaimPayee.Visible = False
            pnlSearchClaimPayee.Visible = False
            pnlSeekLossCode.Visible = False
            pnlLaborDetail.Visible = False
            GetShopRate()
            'FillLossCode()
            If CheckLock() Then
                LockButtons()
            Else
                UnlockButtons()
            End If
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                LockButtons()
            End If
        End If
    End Sub

    Private Sub GetShopRate()
        hfShopLabor.Value = 0
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select shoprate from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfShopLabor.Value = clC.Fields("shoprate")
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Date.Today
        sEndDate = DateAdd("d", 1, Date.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Function CheckLock() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckLock = False
        SQL = "Select claimid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And lockuserid <> " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CheckLock = False
        Else
            CheckLock = True
        End If
    End Function

    Private Sub LockButtons()
        btnAddClaimPayee.Enabled = False
        btnAddLabor.Enabled = False
        btnAddLossCode.Enabled = False
        btnAddPart.Enabled = False
        btnSCSave.Enabled = False
        btnSeekClaimPayeeLabor.Enabled = False
        btnSeekLossCodeLabor.Enabled = False
        btnSeekLossCodePart.Enabled = False
        btnSeekPayee.Enabled = False
        btnUpdateLabor.Enabled = False
        btnUpdatePart.Enabled = False
    End Sub

    Private Sub UnlockButtons()
        btnAddClaimPayee.Enabled = True
        btnAddLabor.Enabled = True
        btnAddLossCode.Enabled = True
        btnAddPart.Enabled = True
        btnSCSave.Enabled = True
        btnSeekClaimPayeeLabor.Enabled = True
        btnSeekLossCodeLabor.Enabled = True
        btnSeekLossCodePart.Enabled = True
        btnSeekPayee.Enabled = True
        btnUpdateLabor.Enabled = True
        btnUpdatePart.Enabled = True
    End Sub

    Private Sub FillLossCode()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select losscodeid, LossCodeDesc, losscode from claimlosscode "
        SQL = SQL + "where losscodeid > 0 "
        SQL = SQL + GetLossCodeFilter()

        dsLossCode.SelectCommand = SQL
        rgLossCode.Rebind()
        'rgLossCode.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        'rgLossCode.DataBind()
    End Sub

    Public Function GetLossCodeFilter() As String
        Dim lContractID As Long
        Dim clR As New clsDBO
        Dim sTemp As String = ""
        Dim SQL As String
        GetLossCodeFilter = ""
        lContractID = GetContractID()
        If lContractID = 0 Then
            Exit Function
        End If

        SQL = "select * from contractsurcharge "
        SQL = SQL + "where contractid = " & lContractID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            Exit Function
        Else
            clR.GetRow()
            If clR.Fields("surchargeid") = "1" Then
                sTemp = sTemp + "or losscode like 'ac101%' "
            End If
            If clR.Fields("surchargeid") = "2" Then
                sTemp = sTemp + "or losscode like 'CL101%' "
            End If
            If clR.Fields("surchargeid") = "3" Then
                sTemp = sTemp + "or losscode like 'AC101%' "
            End If
            If clR.Fields("surchargeid") = "4" Then
                sTemp = sTemp + "or losscode like 'PL101%' "
            End If
            If clR.Fields("surchargeid") = "5" Then
                sTemp = sTemp + "or losscode like 'RF103%' "
            End If
            If clR.Fields("surchargeid") = "6" Then
                sTemp = sTemp + "or losscode like 'EL101%' "
            End If
            If clR.Fields("surchargeid") = "7" Then
                sTemp = sTemp + "or losscode like 'SP101%' "
            End If
            If clR.Fields("surchargeid") = "8" Then
                sTemp = sTemp + "or losscode like 'SP102%' "
            End If
            If clR.Fields("surchargeid") = "9" Then
                sTemp = sTemp + "or losscode like 'PL102%' "
            End If
            If clR.Fields("surchargeid") = "10" Then
                sTemp = sTemp + "or losscode like 'AC103%' "
            End If
            If clR.Fields("surchargeid") = "11" Then
                sTemp = sTemp + "or losscode like 'RF101%' "
            End If
            If clR.Fields("surchargeid") = "12" Then
                sTemp = sTemp + "or losscode like 'RF102%' "
            End If
            'sTemp = sTemp + "or losscode like 'rr%' "
            'sTemp = sTemp + "or losscode like 'zs%' "
            'sTemp = sTemp + "or losscode like 'zl%' "
            'sTemp = sTemp + "or losscode like 'zc%' "
            'sTemp = sTemp + "or losscode like 'zb%' "
            'sTemp = sTemp + "or losscode like 'zat%' "
            'sTemp = sTemp + "or losscode like 'w%' "

            'SQL = "select * from claimlosscodelimit "
            'SQL = SQL + "where programid = " & clR.Fields("programid") & " "
            'SQL = SQL + "and plantypeid = " & clR.Fields("plantypeid")
            'clR.OpenDB(SQL, AppSettings("connstring"))
            'If clR.RowCount > 0 Then
            '    For cnt = 0 To clR.RowCount - 1
            '        clR.GetRowNo(cnt)
            '        sTemp = sTemp + "or losscode like '" & clR.Fields("losscodeprefix") & "%' "
            '    Next
            'End If
        End If
        If sTemp.Length > 0 Then
            sTemp = sTemp.Substring(2, sTemp.Length - 2)
            sTemp = "and (" & sTemp & ") "
            GetLossCodeFilter = sTemp
        End If
    End Function

    Private Function GetContractID() As Long
        GetContractID = 0
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("contractid").Length > 0 Then
                GetContractID = clR.Fields("contractid")
            End If
        End If
    End Function

    Private Sub rgParts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgParts.SelectedIndexChanged
        pnlPartList.Visible = False
        pnlPartDetail.Visible = True
        hfAddPart.Value = "False"
        FillPartDetail()
        FillPartQuotes()
        LockAuthPartText()
    End Sub

    Private Sub FillPartDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        trPartLabor.Visible = False
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & rgParts.SelectedValue
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimDetailID.Value = clR.Fields("claimdetailid")
            txtPartNo.Text = clR.Fields("partno")
            txtPartDesc.Text = clR.Fields("Claimdesc")
            txtReqCost.Text = clR.Fields("reqcost")
            txtReqQty.Text = clR.Fields("reqqty")
            hfJobNo.Value = clR.Fields("jobno")
            txtAuthQty.Text = clR.Fields("AuthQty")
            txtAuthCost.Text = clR.Fields("Authcost")
            txtPartTax.Text = clR.Fields("taxper") * 100
            If CDbl(clR.Fields("taxper")) = 0 Then
                txtPartTax.Text = hfPartTax.Value
            End If
            txtShipping.Text = clR.Fields("shippingcost")
            txtLossCodePart.Text = clR.Fields("losscode")
            txtLossCodePartDesc.Text = GetLossCodeDesc(clR.Fields("losscode"))
            hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
            hfJobNo.Value = clR.Fields("jobno")
            hfLossCodeSeek.Value = "Part"
            GetPartLabor()
            GetPayee()
        End If
    End Sub

    Private Sub GetPartLabor()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = '" & hfClaimID.Value & "' "
        SQL = SQL + "and claimdetailtype = 'Labor' "
        SQL = SQL + "and jobno = '" & hfJobNo.Value & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtLaborReqAmtPart.Text = clR.Fields("reqqty")
            txtLaborReqRatePart.Text = hfShopLabor.Value
            txtLaborAuthAmtPart.Text = clR.Fields("authqty")
            txtLaborAuthRatePart.Text = clR.Fields("authcost")

        End If
    End Sub

    Private Function GetLossCodeDesc(xLossCode As String) As String
        GetLossCodeDesc = ""
        If xLossCode.Length = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimlosscode "
        SQL = SQL + "where losscode = '" & xLossCode & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetLossCodeDesc = clR.Fields("losscodedesc")
        End If
    End Function

    Private Sub GetPayee()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfClaimPayeeID.Value = "" Then
            Exit Sub
        End If
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If hfLossCodeSeek.Value = "Labor" Then
                txtClaimPayeeNameLabor.Text = clR.Fields("payeename")
                txtClaimPayeeNoLabor.Text = clR.Fields("payeeno")
            Else
                txtClaimPayee.Text = clR.Fields("payeename")
                txtPayeeNo.Text = clR.Fields("payeeno")
            End If
        End If
    End Sub

    Private Sub btnAddPart_Click(sender As Object, e As EventArgs) Handles btnAddPart.Click
        hfJobNo.Value = ""
        txtLossCodePart.Text = ""
        CreClaimDetail()
        pnlPartList.Visible = False
        pnlPartDetail.Visible = True
        txtPayeeNo.Text = ""
        txtPartNo.Text = ""
        txtPartDesc.Text = ""
        txtAuthCost.Text = 0
        txtReqQty.Text = 0
        txtReqCost.Text = 0
        txtAuthQty.Text = 0
        txtAuthQty.Text = 0
        txtPayeeNo.Text = ""
        txtClaimPayee.Text = ""
        txtLossCodePart.Text = ""
        txtLossCodePartDesc.Text = ""
        txtShipping.Text = 0
        trPartLabor.Visible = True
        GetClaimPayeeID()
        txtLaborReqAmtPart.Text = hfShopLabor.Value
        SetLossCodePart()
    End Sub

    Private Sub CreClaimDetail()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimdetailtype") = "Part"
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("credate") = Date.Today
            clR.Fields("claimdetailstatus") = "Requested"
            clR.Fields("creby") = hfUserID.Value
            GetNextJobNo()
            clR.Fields("jobno") = hfJobNo.Value
            hfAddPart.Value = "True"
            clR.AddRow()
            clR.SaveDB()
        End If
        SQL = "update claim "
        SQL = SQL + "set moddate = '" & Date.Today & "', "
        SQL = SQL + "modby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "select max(claimdetailid) as mcd from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and creby = " & hfUserID.Value & " "
        SQL = SQL + "and jobno = '" & hfJobNo.Value & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimDetailID.Value = clR.Fields("mcd")
        End If
    End Sub

    Private Sub SetLossCodePart()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select losscode from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtLossCodePart.Text = clR.Fields("losscode")
        End If
        If txtLossCodePart.Text.Length > 0 Then
            SQL = "select * from claimlosscode "
            SQL = SQL + "where losscode = '" & txtLossCodePart.Text & "' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                txtLossCodePartDesc.Text = clR.Fields("losscodedesc")
            End If

        End If
    End Sub

    Private Sub GetClaimPayeeID()
        Dim SQL As String
        Dim clCP As New clsDBO
        Dim clR As New clsDBO
        SQL = "select * from claim c "
        SQL = SQL + "inner join servicecenter sc on sc.servicecenterid = c.servicecenterid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCP.OpenDB(SQL, AppSettings("connstring"))
        If clCP.RowCount > 0 Then
            clCP.GetRow()
            SQL = "select * from claimpayee "
            SQL = SQL + "where payeeno = '" & clCP.Fields("servicecenterno") & "' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
            Else
                clR.NewRow()
                clR.Fields("payeeno") = clCP.Fields("servicecenterno")
                clR.Fields("payeename") = clCP.Fields("servicecentername")
                clR.Fields("addr1") = clCP.Fields("addr1")
                clR.Fields("addr2") = clCP.Fields("addr2")
                clR.Fields("city") = clCP.Fields("city")
                clR.Fields("zip") = clCP.Fields("zip")
                clR.Fields("phone") = clCP.Fields("phone")
                clR.Fields("email") = clCP.Fields("email")
                clR.AddRow()
                clR.SaveDB()
                SQL = "select * from claimpayee "
                SQL = SQL + "where payeeno = '" & clCP.Fields("servicecenterno") & "' "
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 0 Then
                    clR.GetRow()
                    hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
                End If
            End If
        End If
    End Sub

    Private Sub btnSeekPayee_Click(sender As Object, e As EventArgs) Handles btnSeekPayee.Click
        hfLossCodeSeek.Value = "True"
        pnlPartDetail.Visible = False
        pnlSearchClaimPayee.Visible = True
    End Sub

    Private Sub btnCancelPart_Click(sender As Object, e As EventArgs) Handles btnCancelPart.Click
        pnlPartDetail.Visible = False
        pnlPartList.Visible = True
        If hfAddPart.Value = "True" Then
            DeletePart()
        End If
        rgParts.Rebind()
    End Sub

    Private Sub DeletePart()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(claimdetailid) as ClaimDetailID from claimdetail "
        SQL = SQL + "where creby = " & hfUserID.Value & " "
        SQL = SQL + "and claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "delete claimdetail "
            SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid")
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If


    End Sub

    Private Sub btnUpdatePart_Click(sender As Object, e As EventArgs) Handles btnUpdatePart.Click
        UpdatePart()
        hfAddPart.Value = "False"
        rgParts.Rebind()
        pnlPartDetail.Visible = False
        pnlPartList.Visible = True
    End Sub

    Private Sub UpdatePart()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim bAdd As Boolean
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value & " "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            bAdd = False
            GetNextJobNo()
        Else
            clR.NewRow()
            GetNextJobNo()
        End If
        If hfAddPart.Value = "True" Then
            bAdd = True
        End If
        clR.Fields("jobno") = hfJobNo.Value
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("claimdetailtype") = "Part"
        'clR.Fields("claimdetailstatus") = "Requested"
        clR.Fields("reqqty") = txtReqQty.Text
        clR.Fields("partno") = txtPartNo.Text
        clR.Fields("Claimdesc") = txtPartDesc.Text
        clR.Fields("ReqCost") = txtReqCost.Text
        If txtAuthCost.Text.Length = 0 Then
            txtAuthCost.Text = 0
        End If
        clR.Fields("Authcost") = txtAuthCost.Text
        clR.Fields("AuthQty") = txtAuthQty.Text
        If txtPartTax.Text.Length = 0 Then
            txtPartTax.Text = 0
        End If
        clR.Fields("taxPer") = CDbl(txtPartTax.Text) / 100
        If clR.Fields("claimdetailstatus") = "" Then
            clR.Fields("claimdetailstatus") = "Requested"
        End If
        If txtShipping.Text.Length > 0 Then
            clR.Fields("shippingcost") = txtShipping.Text
        End If
        GetTax()
        clR.Fields("losscode") = txtLossCodePart.Text
        Dim dAdjust As Double = 0
        If txtReqQty.Text.Length = 0 Then
            txtReqQty.Text = 0
        End If
        If txtReqCost.Text.Length = 0 Then
            txtReqCost.Text = 0
        End If
        clR.Fields("reqamt") = (CDbl(txtReqCost.Text) * CDbl(txtReqQty.Text)) + CDbl(txtShipping.Text)
        If txtAuthQty.Text.Length = 0 Then
            txtAuthQty.Text = 0
        End If
        If txtAuthCost.Text.Length = 0 Then
            txtAuthCost.Text = 0
        End If
        If txtPartTax.Text.Length = 0 Then
            txtPartTax.Text = 0
        End If
        clR.Fields("ratetypeid") = 1
        clR.Fields("taxamt") = (CDbl(txtAuthCost.Text) * CDbl(txtAuthQty.Text)) * (CDbl(txtPartTax.Text) / 100)
        clR.Fields("taxamt") = Decimal.Round(CDbl(clR.Fields("taxamt")) * 100) / 100
        clR.Fields("authamt") = (CDbl(txtAuthCost.Text) * CDbl(txtAuthQty.Text)) + CDbl(txtShipping.Text)
        clR.Fields("TotalAmt") = CDbl(clR.Fields("authamt")) + CDbl(clR.Fields("taxamt"))
        clR.Fields("claimpayeeid") = hfClaimPayeeID.Value
        clR.Fields("moddate") = Date.Today
        clR.Fields("modby") = hfUserID.Value
MoveHere:
        If clR.RowCount = 0 Then
            clR.Fields("credate") = Date.Today
            clR.Fields("creby") = hfUserID.Value
            clR.AddRow()
        End If
        clR.SaveDB()
        If bAdd = True Then
            SaveLaborPart()
        End If
        rgParts.Rebind()
        rgLabor.Rebind()
        SQL = "update claim "
        SQL = SQL + "set moddate = '" & Date.Today & "', "
        SQL = SQL + "modby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))

    End Sub

    Private Function CheckInspect() As Boolean
        CheckInspect = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select inspect from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("inspect") = "True" Then
                CheckInspect = True
            End If
        End If
    End Function


    Private Sub SaveLaborPart()
        If txtLaborReqRatePart.Text.Length = 0 Then
            Exit Sub
        End If
        If txtLaborReqAmtPart.Text.Length = 0 Then
            Exit Sub
        End If
        If CDbl(txtLaborReqAmtPart.Text) = 0 Then
            Exit Sub
        End If
        If CDbl(txtLaborReqRatePart.Text) = 0 Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = '" & hfJobNo.Value & "' "
        SQL = SQL + "and claimdetailtype = 'Labor' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        If txtLaborAuthAmtPart.Text.Length = 0 Then
            txtLaborAuthAmtPart.Text = 0
        End If
        If txtLaborAuthRatePart.Text.Length = 0 Then
            txtLaborAuthRatePart.Text = 0
        End If
        If txtLaborReqAmtPart.Text.Length = 0 Then
            txtLaborReqAmtPart.Text = 0
        End If
        If txtLaborReqRatePart.Text.Length = 0 Then
            txtLaborReqRatePart.Text = 0
        End If
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("jobno") = hfJobNo.Value
        clR.Fields("claimdetailtype") = "Labor"
        clR.Fields("reqqty") = txtLaborReqAmtPart.Text
        clR.Fields("claimdetailstatus") = "Requested"
        clR.Fields("reqcost") = txtLaborReqRatePart.Text
        clR.Fields("ratetypeid") = 1
        clR.Fields("authcost") = txtLaborAuthRatePart.Text
        clR.Fields("authqty") = txtLaborAuthAmtPart.Text
        GetTax()
        clR.Fields("losscode") = txtLossCodePart.Text
        clR.Fields("reqamt") = CDbl(clR.Fields("reqqty")) * CDbl(clR.Fields("reqcost"))
        If txtLaborTax.Text.Length = 0 Then
            txtLaborTax.Text = "0"
        End If
        clR.Fields("taxamt") = Decimal.Round((CDbl(txtLaborAuthRatePart.Text) * CDbl(txtLaborAuthAmtPart.Text)) * (CDbl(txtLaborTax.Text))) / 100
        clR.Fields("authamt") = CDbl(txtLaborAuthRatePart.Text) * CDbl(txtLaborAuthAmtPart.Text)
        clR.Fields("totalamt") = CDbl(clR.Fields("authamt")) + CDbl(clR.Fields("taxamt"))
        clR.Fields("claimpayeeid") = hfClaimPayeeID.Value
        clR.Fields("claimdesc") = txtPartDesc.Text

        clR.Fields("moddate") = Date.Today
        clR.Fields("modby") = hfUserID.Value
        If clR.RowCount = 0 Then
            clR.Fields("ratetypeid") = 1
            clR.Fields("credate") = Date.Today
            clR.Fields("creby") = hfUserID.Value
            clR.AddRow()
        End If
        clR.SaveDB()

    End Sub

    Private Sub GetTax()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfPartTax.Value = clR.Fields("parttax")
            hfLaborTax.Value = clR.Fields("labortax")
        End If
        If hfPartTax.Value.Length = 0 Then
            hfPartTax.Value = 0
        End If
        If hfLaborTax.Value.Length = 0 Then
            hfLaborTax.Value = 0
        End If

    End Sub

    Private Sub GetClaimDetailID()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = '" & hfJobNo.Value & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimDetailID.Value = clR.Fields("claimdetailid")
        End If
    End Sub

    Private Sub GetNextJobNo()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfJobNo.Value.Length = 0 Then
            SQL = "select max(jobno) as mJob from claimdetail "
            SQL = SQL + "where claimid = " & hfClaimID.Value & " "
            SQL = SQL + "and jobno like 'j%' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount = 0 Then
                hfJobNo.Value = "J01"
            Else
                clR.GetRow()
                If clR.Fields("mjob").Length = 0 Then
                    hfJobNo.Value = "J01"

                Else
                    hfJobNo.Value = "J" & Format(CLng(clR.Fields("mjob").Substring(clR.Fields("mjob").Length - 2, 2)) + 1, "00")
                End If
            End If
        End If

        SQL = "select min(jobno) as mJob from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno like 'j%' "
        If txtLossCodePart.Text.Length > 0 Then
            SQL = SQL + "and losscode = '" & txtLossCodePart.Text.Trim & "' "
        Else
            SQL = SQL + "and losscode is null "
        End If
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("mjob").Length > 0 Then
                hfJobNo.Value = clR.Fields("mJob")
            End If
        End If

    End Sub

    Private Sub GetNextJobNoLabor()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfJobNo.Value.Length = 0 Then
            SQL = "select max(jobno) as mJob from claimdetail "
            SQL = SQL + "where claimid = " & hfClaimID.Value & " "
            SQL = SQL + "and jobno like 'j%' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount = 0 Then
                hfJobNo.Value = "J01"
            Else
                clR.GetRow()
                If clR.Fields("mjob").Length = 0 Then
                    hfJobNo.Value = "J01"

                Else
                    hfJobNo.Value = "J" & Format(CLng(clR.Fields("mjob").Substring(clR.Fields("mjob").Length - 2, 2)) + 1, "00")
                End If
            End If
        End If

        SQL = "select min(jobno) as mJob from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno like 'j%' "
        If txtLossCodeLabor.Text.Length > 0 Then
            SQL = SQL + "and losscode = '" & txtLossCodeLabor.Text.Trim & "' "
        Else
            SQL = SQL + "and losscode is null "
        End If
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("mjob").Length > 0 Then
                hfJobNo.Value = clR.Fields("mJob")
            End If
        End If

    End Sub

    Private Sub btnAddClaimPayee_Click(sender As Object, e As EventArgs) Handles btnAddClaimPayee.Click
        pnlSearchClaimPayee.Visible = False
        pnlAddClaimPayee.Visible = True
        txtServiceCenterName.Text = ""
        txtZip.Text = ""
        txtAddr1.Text = ""
        txtAddr2.Text = ""
        cboState.ClearSelection()
        txtPhone.Text = ""
    End Sub

    Private Sub btnSCCancel_Click(sender As Object, e As EventArgs) Handles btnSCCancel.Click
        pnlSearchClaimPayee.Visible = True
        pnlAddClaimPayee.Visible = False
    End Sub

    Private Sub btnSCSave_Click(sender As Object, e As EventArgs) Handles btnSCSave.Click
        AddServiceCenter()
        AddClaimPayee()
        rgClaimPayee.Rebind()
        pnlSearchClaimPayee.Visible = True
        pnlAddClaimPayee.Visible = False
    End Sub

    Private Sub AddClaimPayee()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        clR.Fields("payeeno") = hfServiceCenterNo.Value
        clR.Fields("payeename") = txtServiceCenterName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("zip") = txtZip.Text
        clR.Fields("phone") = txtPhone.Text
        clR.Fields("fax") = txtFax.Text
        clR.Fields("email") = txtEMail.Text
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub AddServiceCenter()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        GetNextServiceCenterNo()
        clR.Fields("servicecenterno") = hfServiceCenterNo.Value
        clR.Fields("servicecentername") = txtServiceCenterName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("zip") = txtZip.Text
        clR.Fields("phone") = txtPhone.Text
        clR.Fields("fax") = txtFax.Text
        clR.Fields("email") = txtEMail.Text
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub GetNextServiceCenterNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(servicecenterno) as mSCN from servicecenter "
        SQL = SQL + "where servicecenterno like 'RF0%' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.GetRow()
        hfServiceCenterNo.Value = "RF" & Format(CLng(clR.Fields("mscn").Substring(clR.Fields("mscn").Length - 6, 6)) + 1, "0000000")
    End Sub

    Private Sub rgClaimPayee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimPayee.SelectedIndexChanged
        hfClaimPayeeID.Value = rgClaimPayee.SelectedValue
        Dim SQL As String
        Dim clCP As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & rgClaimPayee.SelectedValue
        clCP.OpenDB(SQL, AppSettings("connstring"))
        If clCP.RowCount > 0 Then
            clCP.GetRow()
            If hfLossCodeSeek.Value = "Labor" Then
                txtClaimPayeeNameLabor.Text = clCP.Fields("payeename")
                txtClaimPayeeNoLabor.Text = clCP.Fields("payeeno")
                pnlLaborDetail.Visible = True
                pnlSearchClaimPayee.Visible = False
            Else
                txtClaimPayee.Text = clCP.Fields("payeename")
                txtPayeeNo.Text = clCP.Fields("payeeno")
                pnlPartDetail.Visible = True
                pnlSearchClaimPayee.Visible = False
            End If
        End If
    End Sub

    Private Sub btnSeekLossCodePart_Click(sender As Object, e As EventArgs) Handles btnSeekLossCodePart.Click
        hfLossCodeSeek.Value = "Part"
        pnlPartDetail.Visible = False
        pnlSeekLossCode.Visible = True
        rgLossCode.Rebind()
        ColorLossCode()
    End Sub

    Private Sub rgLossCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgLossCode.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        If hfLossCodeSeek.Value = "Part" Then
            txtLossCodePart.Text = rgLossCode.SelectedValue
            SQL = "select * from claimlosscode "
            SQL = SQL + "where losscode = '" & rgLossCode.SelectedValue & "' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                txtLossCodePartDesc.Text = clR.Fields("losscodedesc")
            End If
            pnlPartDetail.Visible = True
            pnlSeekLossCode.Visible = False
            GetNextJobNo()
            lockauthparttext
        End If
        If hfLossCodeSeek.Value = "Labor" Then
            txtLossCodeLabor.Text = rgLossCode.SelectedValue
            SQL = "select * from claimlosscode "
            SQL = SQL + "where losscode = '" & rgLossCode.SelectedValue & "' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                txtLossCodeDescLabor.Text = clR.Fields("losscodedesc")
            End If
            pnlLaborDetail.Visible = True
            pnlSeekLossCode.Visible = False
            GetNextJobNoLabor()
            lockauthlabortext
        End If

    End Sub

    Private Sub LockAuthLaborText()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("AllCoverage")) Then
                Exit Sub
            End If
        End If

        If GetSaleDate() < CDate("1/1/2019") Then
            Exit Sub
        End If

        SQL = "select * from claimlosscode clc "
        SQL = SQL + "inner join contractcoverage cc "
        SQL = SQL + "on cc.CoverageCode = clc.LossCodePrefix "
        SQL = SQL + "inner join contract c "
        SQL = SQL + "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID "
        SQL = SQL + "inner join claim cl "
        SQL = SQL + "on cl.ContractID = c.ContractID "
        SQL = SQL + "where cl.claimid = " & hfClaimID.Value & " "
        If txtLossCodeLabor.Text.Length = 0 Then
            Exit Sub
        End If
        SQL = SQL + "and cc.coveragecode = '" & txtLossCodeLabor.Text.Substring(0, 4) & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            SQL = "update claimdetail "
            SQL = SQL + "set losscodelock = 1 "
            SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
            txtLaborAuthHours.Enabled = False
            txtLaborAuthRate.Enabled = False
            txtLaborTax.Enabled = False
            txtLossCodeLabor.BackColor = Drawing.Color.Red
        Else
            SQL = "update claimdetail "
            SQL = SQL + "set losscodelock = 0 "
            SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
            txtLaborAuthHours.Enabled = True
            txtLaborAuthRate.Enabled = True
            txtLaborTax.Enabled = True
            txtAuthCost.Enabled = True
            txtLossCodeLabor.BackColor = Drawing.Color.White
        End If
    End Sub

    Private Function GetSaleDate() As Date
        Dim clR As New clsDBO
        Dim SQL As String
        GetSaleDate = "1/1/1950"
        SQL = "select c.saledate from contract c "
        SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetSaleDate = clR.Fields("saledate")
        End If
    End Function

    Private Sub LockAuthPartText()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("AllCoverage")) Then
                Exit Sub
            End If
        End If
        If GetSaleDate() < CDate("1/1/2019") Then
            Exit Sub
        End If

        SQL = "select * from claimlosscode clc "
        SQL = SQL + "inner join contractcoverage cc "
        SQL = SQL + "on cc.CoverageCode = clc.LossCodePrefix "
        SQL = SQL + "inner join contract c "
        SQL = SQL + "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID "
        SQL = SQL + "inner join claim cl "
        SQL = SQL + "on cl.ContractID = c.ContractID "
        SQL = SQL + "where cl.claimid = " & hfClaimID.Value & " "
        If txtLossCodePart.Text.Length = 0 Then
            Exit Sub
        End If
        SQL = SQL + "and cc.coveragecode = '" & txtLossCodePart.Text.Substring(0, 4) & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            SQL = "update claimdetail "
            SQL = SQL + "set losscodelock = 1 "
            SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
            txtAuthCost.Enabled = False
            txtAuthQty.Enabled = False
            txtPartTax.Enabled = False
            txtLaborAuthAmtPart.Enabled = False
            txtLaborAuthRatePart.Enabled = False
            txtLossCodePart.BackColor = Drawing.Color.Red
        Else
            SQL = "update claimdetail "
            SQL = SQL + "set losscodelock = 0 "
            SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
            txtAuthCost.Enabled = True
            txtAuthQty.Enabled = True
            txtPartTax.Enabled = True
            txtLaborAuthAmtPart.Enabled = True
            txtLaborAuthRatePart.Enabled = True
            txtLossCodePart.BackColor = Drawing.Color.White
        End If
    End Sub

    Private Sub btnAddLabor_Click(sender As Object, e As EventArgs) Handles btnAddLabor.Click
        pnlPartList.Visible = False
        pnlLaborDetail.Visible = True
        hfClaimDetailID.Value = 0
        ClearLabor()
        txtLaborReqRate.Text = hfShopLabor.Value
        hfJobNo.Value = ""
        txtLossCodePart.Text = ""
        SetLaborLossCode()
    End Sub

    Private Sub SetLaborLossCode()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select losscode from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtLossCodeLabor.Text = clR.Fields("losscode")
        End If
        If txtLossCodePart.Text.Length > 0 Then
            SQL = "select * from claimlosscode "
            SQL = SQL + "where losscode = '" & txtLossCodePart.Text & "' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                txtLossCodeDescLabor.Text = clR.Fields("losscodedesc")
            End If

        End If
    End Sub

    Private Sub ClearLabor()
        txtClaimPayee.Text = ""
        txtPayeeNo.Text = ""
        txtLaborReqHours.Text = "1"
        txtLaborReqRate.Text = ""
        txtLaborAuthHours.Text = "1"
        txtLaborAuthRate.Text = ""
        hfClaimDetailID.Value = 0
        GetClaimPayeeID()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtClaimPayeeNoLabor.Text = clR.Fields("payeeno")
            txtClaimPayeeNameLabor.Text = clR.Fields("payeename")
        End If
    End Sub

    Private Sub btnSeekLossCodeLabor_Click(sender As Object, e As EventArgs) Handles btnSeekLossCodeLabor.Click
        hfLossCodeSeek.Value = "Labor"
        pnlLaborDetail.Visible = False
        pnlSeekLossCode.Visible = True
        rgLossCode.Rebind()
        ColorLossCode()
    End Sub

    Private Sub btnSeekClaimPayeeLabor_Click(sender As Object, e As EventArgs) Handles btnSeekClaimPayeeLabor.Click
        hfLossCodeSeek.Value = "Labor"
        pnlLaborDetail.Visible = False
        pnlSearchClaimPayee.Visible = True
    End Sub

    Private Sub btnCancelLabor_Click(sender As Object, e As EventArgs) Handles btnCancelLabor.Click
        pnlLaborDetail.Visible = False
        pnlPartList.Visible = True
        rgLabor.Rebind()
    End Sub

    Private Sub btnUpdateLabor_Click(sender As Object, e As EventArgs) Handles btnUpdateLabor.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetNextJobNoLabor()
        Else
            clR.NewRow()
            GetNextJobNoLabor()
            clR.Fields("claimid") = hfClaimID.Value
        End If
        clR.Fields("claimdetailtype") = "Labor"
        If txtLaborReqRate.Text = "" Then
            txtLaborReqRate.Text = 0
        End If
        If txtLaborReqHours.Text = "" Then
            txtLaborReqHours.Text = 0
        End If
        If txtLaborAuthRate.Text = "" Then
            txtLaborAuthRate.Text = 0
        End If
        If txtLaborAuthHours.Text = "" Then
            txtLaborAuthHours.Text = 0
        End If
        If txtLaborTaxRate.Text.Length = 0 Then
            txtLaborTaxRate.Text = 0
        End If
        clR.Fields("jobno") = hfJobNo.Value
        clR.Fields("taxper") = txtLaborTaxRate.Text / 100
        clR.Fields("Reqqty") = txtLaborReqHours.Text
        clR.Fields("reqcost") = txtLaborReqRate.Text
        clR.Fields("Authqty") = txtLaborAuthHours.Text
        clR.Fields("Authcost") = txtLaborAuthRate.Text
        clR.Fields("ratetypeid") = 1
        clR.Fields("losscode") = txtLossCodeLabor.Text
        clR.Fields("reqamt") = CDbl(txtLaborReqHours.Text) * CDbl(txtLaborReqRate.Text)
        If hfLaborTax.Value = "" Then
            hfLaborTax.Value = 0
        End If
        If txtLaborTaxRate.Text.Length = 0 Then
            txtLaborTaxRate.Text = 0
        End If
        clR.Fields("taxper") = CDbl(txtLaborTaxRate.Text / 100)
        clR.Fields("taxamt") = (CDbl(txtLaborAuthHours.Text) * CDbl(txtLaborAuthRate.Text)) * (CDbl(txtLaborTaxRate.Text) / 100)
        clR.Fields("taxamt") = Decimal.Round(CDbl(clR.Fields("taxamt")) * 100) / 100
        clR.Fields("authamt") = (CDbl(txtLaborAuthHours.Text) * CDbl(txtLaborAuthRate.Text))
        clR.Fields("totalamt") = clR.Fields("authamt") + CDbl(clR.Fields("taxamt"))
        clR.Fields("claimpayeeid") = hfClaimPayeeID.Value
        clR.Fields("claimdesc") = txtLaborDesc.Text
        clR.Fields("moddate") = Date.Today
        clR.Fields("modby") = hfUserID.Value
        If clR.Fields("claimdetailstatus") = "Authorized" And Not CheckInspect() Then
            lblAuthorizedError.Text = "Inspection has not been completed."
            pnlPartList.Visible = True
            pnlPartDetail.Visible = False
            pnlLaborDetail.Visible = False
            clR.Fields("claimdetailstatus") = "Requested"
        End If
        If clR.Fields("claimdetailstatus") = "Approved" And Not CheckInspect() Then
            lblAuthorizedError.Text = "Inspection has not been completed."
            pnlPartList.Visible = True
            pnlPartDetail.Visible = False
            pnlLaborDetail.Visible = False
            clR.Fields("claimdetailstatus") = "Requested"
        End If
        If clR.Fields("claimdetailstatus") = "" Then
            clR.Fields("claimdetailstatus") = "Requested"
        End If

        If clR.RowCount = 0 Then
            clR.Fields("credate") = Date.Today
            clR.Fields("creby") = hfUserID.Value
            clR.AddRow()
        End If
        clR.SaveDB()
        pnlPartList.Visible = True
        pnlLaborDetail.Visible = False
        rgLabor.Rebind()
        SQL = "update claim "
        SQL = SQL + "set moddate = '" & Date.Today & "', "
        SQL = SQL + "modby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Function CheckLimitApproved(xAmt As Double) As Boolean
        CheckLimitApproved = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dLimit As Double
        SQL = "select claimapprove from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dLimit = clR.Fields("claimapprove")
        Else
            dLimit = 0
        End If
        SQL = "select sum(authamt) as Amt from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'Authorized') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If dLimit > CDbl(clR.Fields("amt")) Then
                    CheckLimitApproved = True
                End If
            Else
                If dLimit > xAmt Then
                    CheckLimitApproved = True
                End If
            End If
        End If
    End Function

    Private Function CheckLimitAuthorized(xAmt As Double) As Boolean
        CheckLimitAuthorized = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dLimit As Double
        SQL = "select claimauth from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dLimit = clR.Fields("claimauth")
        Else
            dLimit = 0
        End If
        SQL = "select sum(authamt) as Amt from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'Authorized') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If dLimit > CDbl(clR.Fields("amt")) Then
                    CheckLimitAuthorized = True
                End If
            Else
                If dLimit > xAmt Then
                    CheckLimitAuthorized = True
                End If
            End If
        End If
    End Function

    Private Sub rgLabor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgLabor.SelectedIndexChanged
        pnlPartList.Visible = False
        pnlLaborDetail.Visible = True
        hfClaimDetailID.Value = rgLabor.SelectedValue
        FillLaborDetail()
        LockAuthLaborText()
    End Sub

    Private Sub FillLaborDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("qty") = "" Then
                clR.Fields("qty") = 0
            End If
            If clR.Fields("cost") = "" Then
                clR.Fields("cost") = 0
            End If
            txtLaborReqHours.Text = clR.Fields("reqqty")
            txtLaborReqRate.Text = clR.Fields("reqcost")
            txtLaborAuthRate.Text = clR.Fields("authcost")
            txtLaborAuthHours.Text = clR.Fields("authqty")
            txtLaborTaxRate.Text = CDbl(clR.Fields("taxper")) / 100
            If CDbl(clR.Fields("taxper")) = 0 Then
                txtLaborTaxRate.Text = hfLaborTax.Value
            End If
            txtLaborTax.Text = clR.Fields("taxamt")
            If txtLaborAuthHours.Text.Length = 0 Then
                txtLaborAuthHours.Text = 0
            End If
            If txtLaborAuthRate.Text.Length = 0 Then
                txtLaborAuthRate.Text = 0
            End If
            If txtLaborTax.Text.Length = 0 Then
                txtLaborTax.Text = 0
            End If
            txtLossCodeLabor.Text = clR.Fields("losscode")
            txtLossCodeDescLabor.Text = GetLossCodeDesc(clR.Fields("losscode"))
            hfLossCodeSeek.Value = "Labor"
            hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
            txtLaborDesc.Text = clR.Fields("claimdesc")
            GetPayee()
        End If
    End Sub

    Private Sub txtPartNo_TextChanged(sender As Object, e As EventArgs) Handles txtPartNo.TextChanged
        GetPartinfo()
        txtPartDesc.Focus()
        'Dim FP As New VeritasGlobalTools.ForteParts
        'FP.PartNo = txtPartNo.Text
        'FP.VIN = GetVIN()
        'If FP.VIN.Length > 0 Then
        '    FP.ProcessPartNo()
        '    txtPartDesc.Text = FP.PartDesc
        'End If
    End Sub

    Private Sub GetPartinfo()
        Dim sVIN As String
        Dim sMake As String
        Dim sPartDesc As String
        Dim lQty As Long
        If txtReqQty.Text.Length = 0 Then
            lQty = 1
        Else
            lQty = txtReqQty.Text
        End If
        sVIN = hfClaimDetailID.Value
        sVIN = GetVIN()
        sMake = GetMake(sVIN)
        Process.Start("C:\ProcessProgram\CKParts\CKParts.exe", hfClaimDetailID.Value).WaitForExit()
        sPartDesc = GetPartDesc()
        If sPartDesc.Length > 0 Then
            txtPartDesc.Text = sPartDesc
        End If
        FillPartQuotes()
    End Sub

    Private Sub FillPartQuotes()
        'Dim SQL As String
        'Dim clR As New clsDBO
        'SQL = "select claimdetailquoteid, companyname, partno, partdesc, instock, qty, listprice, yourcost, "
        'SQL = SQL + "case when oem <> 0 then "
        'SQL = SQL + "'OEM' else 'Aftermarket' end as QuoteType, partcost, shipping, orderid, totalordercost "
        'SQL = SQL + "from claimdetailquote cdq "
        'SQL = SQL + "inner join claimcompany cc on cc.claimcompanyid = cdq.claimcompanyid "
        'SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        'rgClaimDetailQuote.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        'rgClaimDetailQuote.DataBind()
    End Sub

    Private Function GetPartDesc() As String
        GetPartDesc = ""
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimdesc from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetPartDesc = clR.Fields("claimdesc")
        End If

    End Function

    Private Function GetMake(xVIN As String) As String
        GetMake = ""
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from vin.dbo.vin v "
        SQL = SQL + "inner join vin.dbo.basicdata bd on v.vinid = bd.vinid "
        SQL = SQL + "where v.vin = '" & xVIN.Substring(0, 11) & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetMake = clR.Fields("make")
        End If
    End Function

    Private Function GetVIN() As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetVIN = ""
        SQL = "select vin from contract c "
        SQL = SQL + "inner join claim cl on c.contractid = cl.contractid "
        SQL = SQL + "where cl.claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetVIN = clR.Fields("vin")
        End If
    End Function

    Private Sub btnSendQuote_Click(sender As Object, e As EventArgs) Handles btnSendQuote.Click
        Dim sVIN As String
        Dim clPart As New VeritasGlobalTools.clsParts
        sVIN = GetVIN()
        clPart.PartNo = txtPartNo.Text
        clPart.PartName = txtPartDesc.Text
        clPart.ClaimID = hfClaimID.Value
        clPart.VIN = sVIN
        clPart.SendPartRequest()
    End Sub

    Private Sub ProcessPurchase()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dPartCost As Double
        Dim sJobNo As String = ""
        Dim sLossCode As String = ""
        lblApprovedError.Text = ""

        SQL = "select * from claimdetailquote "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("orderid") <> "" Then
                lblApprovedError.Text = "This part has been ordered all ready"
                Exit Sub
            End If
            If clR.Fields("youcost") < 50 Then
                lblApprovedError.Text = "This part is to low of cost for purchase."
                Exit Sub
            End If
            If Not CheckLimitAuthorized(CDbl(clR.Fields("yourcost")) + 42.5) Then
                lblApprovedError.Text = "You are not authorized to make purchase."
                Exit Sub
            End If
            Process.Start("C:\ProcessProgram\CKPartPurchase\CKPartPurchase.exe", clR.Fields("claimdetailquoteid") & " " & hfUserID.Value).WaitForExit()
            FillPartQuotes()

        End If
        SQL = "select * from claimdetailquote "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("orderid") = "" Then
                Exit Sub
            End If
            dPartCost = clR.Fields("partcost")
        End If

        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("authamt") = dPartCost
            clR.Fields("taxper") = 0
            clR.Fields("taxamt") = 0
            clR.Fields("totalamt") = dPartCost
            clR.Fields("claimdetailstatus") = "Authorized"
            clR.Fields("claimpayeeid") = "7351"
            clR.Fields("dateauth") = DateTime.Today
            clR.Fields("authby") = hfUserID.Value
            sJobNo = clR.Fields("jobno")
            sLossCode = clR.Fields("losscode")
            clR.SaveDB()
        End If
        SQL = "insert into claimdetail "
        SQL = SQL + "(claimid,claimdetailtype, JobNo; reqqty,reqcost,authqty, quthcost,reqamt,losscode,authamt,taxper,taxamt,claimdetailstatus, dateauth, authby, credate, creby, moddate, modby,ClaimDesc, claimpayeeid)"
        SQL = SQL + "values "
        SQL = SQL + "(" & hfClaimID.Value & ",'Part','" & sJobNo & "', 1, 42.50, 1, 42.50, 42.50,'" & sLossCode & "',42.50,0,0, 'Authorized','" & DateTime.Today & "'," & hfUserID.Value & ",'" & DateTime.Today & "'," & hfUserID.Value & ",'" & DateTime.Today & "'," & hfUserID.Value & ",'Shipping',7351"
        clR.RunSQL(SQL, AppSettings("connstring"))

    End Sub

    Private Sub ColorLossCode()
        Dim SQL As String
        Dim clR As New clsDBO

        If GetSaleDate() < CDate("1/1/2019") Then
            Exit Sub
        End If


        rgLossCode.Columns(3).Visible = True

        For cnt2 = 0 To rgLossCode.Items.Count - 1
            If rgLossCode.Items(cnt2).Item("ACP").Text = "1" Then
                rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.Red
            End If
        Next

        rgLossCode.Columns(3).Visible = False

        'SQL = "select coveragecode from contractcoverage cc "
        'SQL = SQL + "inner join contract c on c.programid = cc.programid and c.plantypeid = cc.plantypeid "
        'SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
        'SQL = SQL + "where cl.claimid = " & hfClaimID.Value
        'clR.OpenDB(SQL, AppSettings("connstring"))
        'If clR.RowCount > 0 Then
        '    For cnt = 0 To clR.RowCount - 1
        '        clR.GetRowNo(cnt)
        '        For cnt2 = 0 To rgLossCode.Items.Count - 1
        '            If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = clR.Fields("coveragecode") Then
        '                rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '            End If
        '            If hfUserID.Value = "1" Or hfUserID.Value = "1205" Or hfUserID.Value = "1205" Then
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "RR00" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "PA00" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZLEG" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZSET" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZATO" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '            End If
        '        Next
        '    Next
        'End If
    End Sub
End Class