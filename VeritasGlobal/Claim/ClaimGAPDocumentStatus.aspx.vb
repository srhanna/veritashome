﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimGAPDocumentStatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("ClaimID")
        hfID.Value = Request.QueryString("sid")
        If Not IsPostBack Then
            GetServerInfo()
            GetInfo()
        End If
    End Sub

    Private Sub GetInfo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocstatus "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            chk60DayLetter.Checked = CBool(clR.Fields("gap60dayletter"))
            chkDenialLetter.Checked = CBool(clR.Fields("gapdenielletter"))
            chkGAPClaimNotice.Checked = CBool(clR.Fields("gapclaimnotice"))
            chkGAPNoGAPDueLetter.Checked = CBool(clR.Fields("gapnogapdueletter"))
            chkMissedOptions.Checked = CBool(clR.Fields("gapmissedoptions"))
            chkPaymentHistory.Checked = CBool(clR.Fields("paymenthistory"))
            chkPaymentLetter.Checked = CBool(clR.Fields("gappaymentletter"))
            chkPoliceFireReport.Checked = CBool(clR.Fields("policefirereport"))
            chkProofInsPayments.Checked = CBool(clR.Fields("ProofInsPayments"))
            chkRISC.Checked = CBool(clR.Fields("risc"))
            chkStatusLetter.Checked = CBool(clR.Fields("gapstatusletter"))
            chkTotalLossInsSettlementLetter.Checked = CBool(clR.Fields("TotalLossSettlement"))
            chkTotalLossInsValuation.Checked = CBool(clR.Fields("TotalLossValuation"))
        Else
            chk60DayLetter.Checked = False
            chkDenialLetter.Checked = False
            chkGAPClaimNotice.Checked = False
            chkGAPNoGAPDueLetter.Checked = False
            chkMissedOptions.Checked = False
            chkPaymentHistory.Checked = False
            chkPaymentLetter.Checked = False
            chkPoliceFireReport.Checked = False
            chkProofInsPayments.Checked = False
            chkRISC.Checked = False
            chkStatusLetter.Checked = False
            chkTotalLossInsSettlementLetter.Checked = False
            chkTotalLossInsValuation.Checked = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocstatus "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
        End If
        clR.Fields("risc") = chkRISC.Checked
        clR.Fields("paymenthistory") = chkPaymentHistory.Checked
        clR.Fields("totallossvaluation") = chkTotalLossInsValuation.Checked
        clR.Fields("totallosssettlement") = chkTotalLossInsSettlementLetter.Checked
        clR.Fields("policefirereport") = chkPoliceFireReport.Checked
        clR.Fields("proofinspayments") = chkProofInsPayments.Checked
        clR.Fields("gapclaimnotice") = chkGAPClaimNotice.Checked
        clR.Fields("gap60dayletter") = chk60DayLetter.Checked
        clR.Fields("gapdenielletter") = chkDenialLetter.Checked
        clR.Fields("gapmissedoptions") = chkMissedOptions.Checked
        clR.Fields("gapnogapdueletter") = chkGAPNoGAPDueLetter.Checked
        clR.Fields("gappaymentletter") = chkPaymentLetter.Checked
        clR.Fields("gapstatusletter") = chkStatusLetter.Checked
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub btnDenialLetter_Click(sender As Object, e As EventArgs) Handles btnDenialLetter.Click
        Dim clD As New VeritasGlobalTools.clsGAPDocuments
        clD.ClaimGapID = hfClaimID.Value
        clD.UserID = hfUserID.Value
        clD.DirLoc = HttpContext.Current.Server.MapPath("~")
        clD.CreateDenialLetter()
    End Sub

    Private Sub btnGAPNoGAPDueLetter_Click(sender As Object, e As EventArgs) Handles btnGAPNoGAPDueLetter.Click
        Dim clD As New VeritasGlobalTools.clsGAPDocuments
        clD.ClaimGapID = hfClaimID.Value
        clD.UserID = hfUserID.Value
        clD.DirLoc = HttpContext.Current.Server.MapPath("~")
        clD.CreateGAPNoGAPDueLetter()
    End Sub

    Private Sub btnGenerate60DayLetter_Click(sender As Object, e As EventArgs) Handles btnGenerate60DayLetter.Click
        Dim clD As New VeritasGlobalTools.clsGAPDocuments
        clD.ClaimGapID = hfClaimID.Value
        clD.UserID = hfUserID.Value
        clD.DirLoc = HttpContext.Current.Server.MapPath("~")
        clD.CreateGAP60DayLetter()
    End Sub

    Private Sub btnGenerateClaimNotice_Click(sender As Object, e As EventArgs) Handles btnGenerateClaimNotice.Click
        Dim clD As New VeritasGlobalTools.clsGAPDocuments
        clD.ClaimGapID = hfClaimID.Value
        clD.UserID = hfUserID.Value
        clD.DirLoc = HttpContext.Current.Server.MapPath("~")
        clD.UserID = hfUserID.Value
        clD.CreateGAPClaimNotice()
    End Sub

    Private Sub btnMissedOptions_Click(sender As Object, e As EventArgs) Handles btnMissedOptions.Click
        Dim clD As New VeritasGlobalTools.clsGAPDocuments
        clD.ClaimGapID = hfClaimID.Value
        clD.UserID = hfUserID.Value
        clD.DirLoc = HttpContext.Current.Server.MapPath("~")
        clD.CreateMissedOptions()
    End Sub

    Private Sub btnPaymentLetter_Click(sender As Object, e As EventArgs) Handles btnPaymentLetter.Click
        Dim clD As New VeritasGlobalTools.clsGAPDocuments
        clD.ClaimGapID = hfClaimID.Value
        clD.UserID = hfUserID.Value
        clD.DirLoc = HttpContext.Current.Server.MapPath("~")
        clD.CreatePaymentLetter()
    End Sub

    Private Sub btnStatusLetter_Click(sender As Object, e As EventArgs) Handles btnStatusLetter.Click
        Dim clD As New VeritasGlobalTools.clsGAPDocuments
        clD.ClaimGapID = hfClaimID.Value
        clD.UserID = hfUserID.Value
        clD.DirLoc = HttpContext.Current.Server.MapPath("~")
        clD.CreateStatusLetter()
    End Sub
End Class