﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI
Imports System.IO

Public Class ClaimDocument1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        hfClaimID.Value = Request.QueryString("claimid")
        dsDocs.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            pnlList.Visible = True
            pnlAdd.Visible = False
            pnlDetail.Visible = False
            FillGrid()
            If CheckLock() Then
                btnAdd.Visible = False
                btnSave.Visible = False
                btnUpload.Visible = False
            Else
                btnAdd.Visible = True
                btnSave.Visible = True
                btnUpload.Visible = True
            End If
            ReadOnlyButtons()
            rgClaimDocument.Rebind()
        End If

    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnAdd.Enabled = False
                btnSave.Enabled = False
                btnUpload.Enabled = False
            End If
        End If
    End Sub

    Private Function CheckLock() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckLock = False
        SQL = "Select claimid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And lockuserid <> " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CheckLock = False
        Else
            CheckLock = True
        End If
    End Function

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillGrid()
        'Dim clR As New clsDBO
        'Dim SQL As String
        'SQL = "select claimdocumentid, documentname, documentdesc, documentlink, credate "
        'SQL = SQL + "from claimdocument "
        'SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        'SQL = SQL + "and deleted = 0 "
        'rgClaimDocument.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgClaimDocument.Rebind()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlAdd.Visible = True
        pnlList.Visible = False
    End Sub

    Private Sub btnUp_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim sDocLink As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetClaimNo()
        Dim folderPath As String = Server.MapPath("~") & "\documents\claims\" & hfClaimNo.Value & "_" & FileUpload2.FileName
        sDocLink = "~/documents/claims/" & hfClaimNo.Value & "_" & FileUpload2.FileName
        FileUpload2.SaveAs(folderPath)

        SQL = "select * from claimdocument "
        SQL = SQL + "where claimid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("documentname") = txtDocName.Text
        clR.Fields("documentdesc") = txtDocDesc.Text
        clR.Fields("documentlink") = sDocLink
        clR.Fields("creby") = hfUserID.Value
        clR.Fields("credate") = Now
        clR.Fields("moddate") = Now
        clR.Fields("modby") = hfUserID.Value
        clR.AddRow()
        clR.SaveDB()
        pnlAdd.Visible = False
        pnlList.Visible = True
        FillGrid()
    End Sub

    Private Sub GetClaimNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimno from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimNo.Value = clR.Fields("claimno")
        End If
    End Sub

    Private Sub rgClaimDocument_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimDocument.SelectedIndexChanged
        pnlList.Visible = False
        pnlDetail.Visible = True
        hfDocID.Value = rgClaimDocument.SelectedValue
        FillDetail()
    End Sub

    Private Sub FillDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdocument "
        SQL = SQL + "where claimdocumentid = " & hfDocID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtCreBy.Text = GetUserInfo(clR.Fields("creby"))
            txtCreDate.Text = clR.Fields("credate")
            txtDescDetail.Text = clR.Fields("documentdesc")
            txtModBy.Text = GetUserInfo(clR.Fields("modby"))
            txtTitleDetail.Text = clR.Fields("documentname")
            txtModDate.Text = clR.Fields("moddate")
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claimdocument "
        SQL = SQL + "set deleted = 1 "
        SQL = SQL + "where claimdocumentid = " & hfDocID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdocument "
        SQL = SQL + "where claimdocumentid = " & hfDocID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("documentname") = txtTitleDetail.Text
            clR.Fields("documentdesc") = txtDescDetail.Text
            clR.Fields("modby") = hfUserID.Value
            clR.Fields("moddate") = Now
            clR.SaveDB()
        End If
        pnlAdd.Visible = False
        pnlList.Visible = True
        rgClaimDocument.Rebind()
    End Sub

    Private Sub btnCloseAdd_Click(sender As Object, e As EventArgs) Handles btnCloseAdd.Click
        pnlAdd.Visible = False
        pnlList.Visible = True
    End Sub
End Class