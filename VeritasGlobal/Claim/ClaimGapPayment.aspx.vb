﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI.Calendar

Public Class ClaimGapPayment
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("ClaimID")
        hfID.Value = Request.QueryString("sid")
        dsClaimPayment.ConnectionString = AppSettings("connstring")
        dsPaymentType.ConnectionString = AppSettings("connstring")
        dsStates.ConnectionString = AppSettings("connstring")
        dsPayee.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            pnlError.Visible = False
            pnlDetail.Visible = False
            pnlSeekPayee.Visible = False
            pnlAddClaimPayee.Visible = False
            rgClaimPayment.Rebind()

        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnAddPayment_Click(sender As Object, e As EventArgs) Handles btnAddPayment.Click
        pnlList.Visible = False
        pnlDetail.Visible = True
        txtPayeeNo.Text = ""
        txtPayeeName.Text = ""
        txtPaymentAuth.Text = 0
        rdpAuthDate.Clear()
        rdpPaidDate.Clear()
        hfClaimPaymentID.Value = 0
    End Sub

    Private Sub rgClaimPayment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimPayment.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        hfClaimPaymentID.Value = rgClaimPayment.SelectedValue
        pnlList.Visible = False
        pnlDetail.Visible = True
        SQL = "select * from claimgappayment "
        SQL = SQL + "where claimpaymentid = " & hfClaimPaymentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfPayeeID.Value = clR.Fields("claimgappayeeid")
            cboPaymentType.SelectedValue = clR.Fields("ClaimGAPPaymentTypeID")
            txtPaymentAuth.Text = clR.Fields("paymentauth")
            If clR.Fields("authdate").Length > 0 Then
                rdpAuthDate.SelectedDate = clR.Fields("authdate")
            Else
                rdpAuthDate.Clear()
            End If
            If clR.Fields("paiddate").Length > 0 Then
                rdpPaidDate.SelectedDate = clR.Fields("paiddate")
            Else
                rdpPaidDate.Clear()
            End If
            hfClaimPaymentID.Value = clR.Fields("claimpaymentid")
            GetPayeeInfo()
            getbankvin
        End If
    End Sub

    Private Sub GetBankVIN()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select * from claimgap "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select right(vin, 6) as Right6 from contract "
            SQL = SQL + "where contractid = " & clR.Fields("contractid")
            clC.OpenDB(SQL, AppSettings("connstring"))
            If clC.RowCount > 0 Then
                clC.GetRow()
                txtLast6.Text = clC.Fields("right6")
            End If
            SQL = "select * from claimgaploaninfo "
            SQL = SQL + "where claimgapid = " & hfClaimID.Value
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                txtBankLoad.Text = clR.Fields("AccountNo")
            End If
        End If
    End Sub

    Private Sub GetPayeeInfo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgappayee "
        SQL = SQL + "where claimgappayeeid = " & hfPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtPayeeNo.Text = clR.Fields("payeeno")
            txtPayeeName.Text = clR.Fields("payeename")
        Else
            txtPayeeNo.Text = ""
            txtPayeeName.Text = ""
        End If
    End Sub

    Private Sub btnSeekPayee_Click(sender As Object, e As EventArgs) Handles btnSeekPayee.Click
        pnlDetail.Visible = False
        pnlSeekPayee.Visible = True
        rgClaimPayee.Rebind()
    End Sub

    Private Sub btnDetailClose_Click(sender As Object, e As EventArgs) Handles btnDetailClose.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgClaimPayment.Rebind()
    End Sub

    Private Sub btnDetailUpdate_Click(sender As Object, e As EventArgs) Handles btnDetailUpdate.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgappayment "
        SQL = SQL + "where claimpaymentid = " & hfClaimPaymentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("moddate") = Today
            clR.Fields("modby") = hfUserID.Value
        Else
            clR.NewRow()
            clR.Fields("credate") = Today
            clR.Fields("creby") = hfUserID.Value
            If hfAuthDate.Value.Length > 0 Then
                clR.Fields("authdate") = hfAuthDate.Value
                clR.Fields("authby") = hfUserID.Value
            End If
        End If
        clR.Fields("claimgapid") = hfClaimID.Value
        clR.Fields("claimgappaymenttypeid") = cboPaymentType.SelectedValue
        clR.Fields("claimgappayeeid") = hfPayeeID.Value
        clR.Fields("paymentauth") = txtPaymentAuth.Text
        If Not rdpAuthDate.SelectedDate Is Nothing Then
            clR.Fields("authdate") = rdpAuthDate.SelectedDate
        End If
        If Not rdpPaidDate.SelectedDate Is Nothing Then
            clR.Fields("paiddate") = rdpPaidDate.SelectedDate
        End If
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub rdpAuthDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpAuthDate.SelectedDateChanged
        hfAuthDate.Value = rdpAuthDate.SelectedDate
        If hfClaimPaymentID.Value.Length = 0 Then
            Exit Sub
        End If
        If hfClaimPaymentID.Value = 0 Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claimpayment "
        SQL = SQL + "set authdate = '" & rdpAuthDate.SelectedDate & "', "
        SQL = SQL + "authby = " & hfUserID.Value & " "
        SQL = SQL + "where claimpaymentid = " & hfClaimPaymentID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub btnAddClaimPayee_Click(sender As Object, e As EventArgs) Handles btnAddClaimPayee.Click
        pnlSeekPayee.Visible = False
        pnlAddClaimPayee.Visible = True
        hfPayeeID.Value = 0
        GetNextPayeeNo()
    End Sub

    Private Sub rdpPaidDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpPaidDate.SelectedDateChanged
        hfAuthDate.Value = rdpAuthDate.SelectedDate
        If hfClaimPaymentID.Value.Length = 0 Then
            Exit Sub
        End If
        If hfClaimPaymentID.Value = 0 Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claimpayment "
        SQL = SQL + "set paiddate = '" & rdpAuthDate.SelectedDate & "', "
        SQL = SQL + "paidby = " & hfUserID.Value & " "
        SQL = SQL + "where claimpaymentid = " & hfClaimPaymentID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub btnPayeeCancel_Click(sender As Object, e As EventArgs) Handles btnPayeeCancel.Click
        pnlAddClaimPayee.Visible = False
        pnlSeekPayee.Visible = True
        rgClaimPayee.Rebind()
    End Sub

    Private Sub btnPayeeSave_Click(sender As Object, e As EventArgs) Handles btnPayeeSave.Click
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claimgappayee "
        SQL = SQL + "where claimgappayeeid = " & hfPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
        Else
            clR.GetRow()
        End If
        clR.Fields("payeeno") = txtGAPPayeeNo.Text
        clR.Fields("payeename") = txtGapPayeeName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("phone") = txtPhone.Text
        clR.Fields("email") = txtEMail.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
        pnlAddClaimPayee.Visible = False
        pnlSeekPayee.Visible = True
        rgClaimPayee.Rebind()
    End Sub

    Private Sub GetNextPayeeNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(payeeno) as mSCN from claimgappayee "
        SQL = SQL + "where payeeno like 'GP0%' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.GetRow()
        If clR.Fields("mscn").Length = 0 Then
            txtGAPPayeeNo.Text = "GP0000000"
        Else
            txtGAPPayeeNo.Text = "GP" & Format(CLng(Right(clR.Fields("mscn"), 6)) + 1, "0000000")
        End If
    End Sub

    Private Sub rgClaimPayee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimPayee.SelectedIndexChanged
        hfPayeeID.Value = rgClaimPayee.SelectedValue
        GetPayeeInfo()
        pnlSeekPayee.Visible = False
        pnlDetail.Visible = True
    End Sub
End Class