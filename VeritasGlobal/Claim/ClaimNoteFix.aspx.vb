﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimNoteFix
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        End If
    End Sub

    Private Sub btnClean_Click(sender As Object, e As EventArgs) Handles btnClean.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimnotetypeid = 5 "
        SQL = SQL + "and (notetext like '%<p%>' "
        SQL = SQL + "or notetext like '%<div%') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                processnote(clR.Fields("claimnoteid"))
            Next
        End If
    End Sub

    Private Sub ProcessNote(xClaimNoteID As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimnoteid = " & xClaimNoteID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtNote.Content = clR.Fields("note")
            clR.Fields("notetext") = txtNote.Text
            clR.SaveDB()
        End If
    End Sub
End Class