﻿Public Class ClaimTeamOpen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetServerInfo()
            CheckToDo()
            FillScreen()
            btnRFClaimSubmit.Enabled = False
            CheckRFClaimSubmit()
            CheckSecurity()
        End If

        If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
        Else
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
        End If
    End Sub

    Private Sub CheckSecurity()
        Dim SQL As String
        Dim clUSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clUSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clUSI.RowCount > 0 Then
            clUSI.GetRow()
            If CBool(clUSI.Fields("AllowClaimAudit")) Then
                btnClaimAudit.Enabled = True
            Else
                btnClaimAudit.Enabled = False
            End If
        End If
    End Sub

    Private Sub CheckRFClaimSubmit()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and teamlead <> 0 "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            SQL = "select * from veritasclaims.dbo.claim "
            SQL = SQL + "where not sendclaim is null "
            SQL = SQL + "and processclaimdate is null "
            clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clR.RowCount > 0 Then
                btnRFClaimSubmit.Enabled = True
            End If
        End If
    End Sub

    Private Sub FillScreen()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clP As New clsDBO
        Dim lTeamID As String = "0"
        SQL = "select * from userinfo ui inner join usersecurityinfo usi on ui.UserID = usi.Userid where ui.userid = " & hfUserID.Value
        clP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clP.RowCount > 0 Then
            clP.GetRow()
            lTeamID = clP.Fields("teamid")
        End If
        SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, 
            cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname,
			case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
            case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			sc.ServiceCenterName, cl.OpenDate, 
			case when DC.cnt is null then 0 else dc.cnt end as opendays,
			max(ci.RequestDate) as InspectionRequest,
			max(um.MessageDate) as InspectionComplete,
            max(case when ci.RequestDate is null then 0 
			else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end 
			end) as InspectionDays
			from claim cl
            inner join contract c on c.contractid = cl.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            left join agents a on d.agentsid = a.agentid
            left join SubAgents sa on sa.SubAgentID = d.SubAgentID
			left join vwClaimAge vcd on cl.ClaimID = vcd.claimid
			left join vwNoActivity vna on cl.claimid = vna.claimid
			left join vwclaimopen vco on cl.claimid = vco.claimid 
            left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
            left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
            left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			left join UserMessage um on ci.InspectionID = um.InspectionID
            left join userinfo ui on ui.userid = cl.assignedto
			left join (select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory
            group by claimid) DC on cl.claimid = dc.claimid
            where ((cl.CloseDate is null
            and not cl.ClaimActivityID in (11,14,15,17,18,21,22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,40,42,43,45,46,47,48)
            and cl.Status = 'Open')
            or cl.claimid in (select claimid from vwClaimDetailRequested)) "

        SQL = SQL + "and AssignedTo in (select ui.userid from userinfo ui inner join usersecurityinfo usi on ui.UserID = usi.Userid where TeamID = "
        SQL = SQL + lTeamID & ") "
        SQL = SQL + "group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate, 
            cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname,
			case when DC.cnt is null then 0 else dc.cnt end,
            case when vna.cnt is null then 0 else vna.cnt end,
			sc.ServiceCenterName, cl.OpenDate, 
			case when vco.cnt is null then 0 else vco.cnt end,
			case when vcd.cnt is null then 0 else vcd.cnt end "
        SQL = SQL + "order by ClaimAge desc "
        rgClaimOpen.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgClaimOpen.Rebind()
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If

    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                btnInspectionWex.Enabled = False
                btnCarfaxPayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub ShowAlert()
        hfAlert.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwAlert.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click

        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click

        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click

        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click

        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click

        Response.Redirect("~/claim/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click

        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub rgClaimOpen_DataBound(sender As Object, e As EventArgs) Handles rgClaimOpen.DataBound
        Dim cnt As Integer
        For cnt = 0 To rgClaimOpen.Items.Count - 1
            If IsNumeric(rgClaimOpen.Items(cnt).Item("ClaimAge").Text) Then
                If rgClaimOpen.Items(cnt).Item("ClaimAge").Text = 3 Then
                    rgClaimOpen.Items(cnt).Item("claimage").BackColor = Drawing.Color.Yellow
                End If
                If rgClaimOpen.Items(cnt).Item("ClaimAge").Text = 4 Then
                    rgClaimOpen.Items(cnt).Item("claimage").BackColor = Drawing.Color.Yellow
                End If
                If rgClaimOpen.Items(cnt).Item("ClaimAge").Text = 5 Then
                    rgClaimOpen.Items(cnt).Item("claimage").BackColor = Drawing.Color.Orange
                End If
                If rgClaimOpen.Items(cnt).Item("ClaimAge").Text = 6 Then
                    rgClaimOpen.Items(cnt).Item("claimage").BackColor = Drawing.Color.Orange
                End If
                If rgClaimOpen.Items(cnt).Item("ClaimAge").Text > 6 Then
                    rgClaimOpen.Items(cnt).Item("claimage").BackColor = Drawing.Color.Red
                End If
            End If
        Next
    End Sub

    Private Sub rgClaimOpen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimOpen.SelectedIndexChanged
        Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & rgClaimOpen.SelectedValue)
    End Sub

    Private Sub btnClaimTeamOpen_Click(sender As Object, e As EventArgs) Handles btnClaimTeamOpen.Click
        Response.Redirect("~/claim/claimteamopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRFClaimSubmit_Click(sender As Object, e As EventArgs) Handles btnRFClaimSubmit.Click
        Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimAudit_Click(sender As Object, e As EventArgs) Handles btnClaimAudit.Click
        Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketMessage_Click(sender As Object, e As EventArgs) Handles btnTicketMessage.Click
        Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketResponse_Click(sender As Object, e As EventArgs) Handles btnTicketResponse.Click
        Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" & hfID.Value)
    End Sub

End Class