﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ClaimJobs1

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnlList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlList As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''tcManagerAuth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tcManagerAuth As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''btnAddDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddDetail As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''cboJobNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboJobNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnAuthIt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAuthIt As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnApproveIt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnApproveIt As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnDenyIt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDenyIt As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''tcManageSlush control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tcManageSlush As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''cboSlushJobNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSlushJobNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboSlushRateType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSlushRateType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnUpdateSlush control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdateSlush As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblAuthorizedError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAuthorizedError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblApprovedError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblApprovedError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rgJobs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgJobs As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''dsJobs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsJobs As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''btnHiddenList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenList As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''cboPayeeTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPayeeTotal As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboJobsTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboJobsTotal As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rgClaimTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgClaimTotal As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''pnlDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDetail As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboClaimDetailType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboClaimDetailType As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''dsCDT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsCDT As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''txtJobNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtJobNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboClaimDetailStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboClaimDetailStatus As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''dsClaimDetailStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsClaimDetailStatus As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''cboReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboReason As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''dsClaimReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsClaimReason As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''txtPartNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPartNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtClaimDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtClaimDesc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtLossCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLossCode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSeekLossCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSeekLossCode As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtLossCodeDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLossCodeDesc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPayeeNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayeeNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPayeeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayeeName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSeekPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSeekPayee As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtReqQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReqQty As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtReqCost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReqCost As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtReqAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReqAmt As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtAuthQty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAuthQty As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtAuthCost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAuthCost As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtTaxRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTaxRate As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtTaxAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTaxAmt As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtTotalAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalAmt As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtAuthAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAuthAmt As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''txtPaidAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPaidAmt As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''rdpAuthorizedDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdpAuthorizedDate As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''rdpDateApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdpDateApprove As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''rdpDatePaid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdpDatePaid As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''cboRateType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboRateType As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''btnHiddenDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenDetail As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnMakePayment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnMakePayment As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCancelClaimDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancelClaimDetail As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSaveClaimDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveClaimDetail As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlLossCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlLossCode As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''rgLossCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgLossCode As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''dsLossCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsLossCode As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''btnHiddenLC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenLC As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSeekPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSeekPayee As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnAddClaimPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddClaimPayee As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rgClaimPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgClaimPayee As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''dsClaimPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsClaimPayee As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''btnHiddenCP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenCP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlAddClaimPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAddClaimPayee As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtServiceCenterName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServiceCenterName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAddr1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddr1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAddr2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddr2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboState As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''dsStates control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsStates As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''txtZip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtZip As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPhone As Global.Telerik.Web.UI.RadMaskedTextBox

    '''<summary>
    '''btnHiddenACP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenACP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSCCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSCCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSCSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSCSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlToBePaidDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlToBePaidDetail As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtClaimPayeeNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtClaimPayeeNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtClaimPayeeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtClaimPayeeName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPayAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayAmt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboPaymentMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPaymentMethod As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''trACH control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trACH As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''txtConfirmNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtConfirmNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnProcessACH control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnProcessACH As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''trCheck control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trCheck As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''txtCheckNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCheckNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnProcessCheck control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnProcessCheck As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''trCC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trCC As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''txtCCNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCCNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnProcessCC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnProcessCC As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''trWex control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trWex As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''cboWexMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboWexMethod As Global.Telerik.Web.UI.RadDropDownList

    '''<summary>
    '''dsWexMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsWexMethod As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''txtWexAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWexAddress As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnProcessWex control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnProcessWex As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCloseToBePaidDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloseToBePaidDetail As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfSlushNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfSlushNote As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''rwSlushNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rwSlushNote As Global.Telerik.Web.UI.RadWindow

    '''<summary>
    '''Label1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtSlushNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSlushNote As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnCloseSlushNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloseSlushNote As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSaveSlushNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveSlushNote As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfFieldCheck control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfFieldCheck As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''rwFieldCheck control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rwFieldCheck As Global.Telerik.Web.UI.RadWindow

    '''<summary>
    '''txtFieldChk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFieldChk As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfUserID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfUserID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfClaimID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfClaimDetailID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimDetailID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfClaimPayeeID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimPayeeID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfJobNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfJobNo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfServiceCenterNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfServiceCenterNo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfClaimPaymentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimPaymentID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfWexCCno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfWexCCno As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfWexCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfWexCode As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfCompanyNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCompanyNo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfClaimNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimNo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfRONo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfRONo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfContractNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfContractNo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfCustomerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCustomerName As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfVIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfVIN As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfPayeeContact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfPayeeContact As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfContractID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfContractID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfTicket control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfTicket As Global.System.Web.UI.WebControls.HiddenField
End Class
