﻿Imports Telerik.Web.UI

Public Class Fulfillment
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsItem.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsOrder1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsOrder2.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsOrder3.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsOrder4.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsOrder5.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            TabStrip1.Tabs(0).Selected = True
        End If
        If System.Configuration.ConfigurationManager.AppSettings("connstring") = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E" Then
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
        Else
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
        End If

        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            Unlockbuttons()
        End If
        Dim clUI As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clUI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clUI.RowCount > 0 Then
            clUI.GetRow()
            hfUserEMail.Value = clUI.Fields("email")
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub TabStrip1_TabClick(sender As Object, e As RadTabStripEventArgs) Handles TabStrip1.TabClick
        If e.Tab.Value = "Approve" Then
            rgOrder.DataSourceID = "dsOrder1"
        End If
        If e.Tab.Value = "Pend" Then
            rgOrder.DataSourceID = "dsOrder2"
        End If
        If e.Tab.Value = "Wait" Then
            rgOrder.DataSourceID = "dsOrder3"
        End If
        If e.Tab.Value = "Shipped" Then
            rgOrder.DataSourceID = "dsOrder4"
        End If
        If e.Tab.Value = "Cancel" Then
            rgOrder.DataSourceID = "dsOrder5"
        End If
    End Sub

    Private Sub btnCancelOrder_Click(sender As Object, e As EventArgs) Handles btnCancelOrder.Click
        Dim dgi As Telerik.Web.UI.GridDataItem
        Dim sOrderNo As String
        Dim clSO As New clsDBO
        Dim SQL As String
        For Each dgi In rgOrder.SelectedItems
            sOrderNo = dgi.Item("OrderNo").Text
            SQL = "select * from shiporder "
            SQL = SQL + "where orderno = '" & sOrderNo & "' "
            'SQL = SQL + "and shiporderstatusid = 1 "
            clSO.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clSO.RowCount > 0 Then
                clSO.GetRow()
                clSO.Fields("shiporderstatusid") = 5
                clSO.SaveDB()
            End If
        Next
    End Sub

    Private Sub btnApproveOrders_Click(sender As Object, e As EventArgs) Handles btnApproveOrders.Click
        Dim dgi As Telerik.Web.UI.GridDataItem
        Dim sOrderNo As String
        Dim clSO As New clsDBO
        Dim SQL As String
        For Each dgi In rgOrder.SelectedItems
            sOrderNo = dgi.Item("OrderNo").Text
            SQL = "select * from shiporder "
            SQL = SQL + "where orderno = '" & sOrderNo & "' "
            SQL = SQL + "and (shiporderstatusid = 1 "
            SQL = SQL + "or shiporderstatusid = 5) "
            clSO.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clSO.RowCount > 0 Then
                clSO.GetRow()
                clSO.Fields("ShipOrderStatusid") = 2
                clSO.Fields("authdate") = Today
                clSO.Fields("authtoship") = hfUserEMail.Value
                ProcessItems(clSO.Fields("orderid"), clSO.Fields("idno"))

                clSO.SaveDB()
            End If
        Next
        rgOrder.Rebind()
        rgItem.Rebind()
    End Sub

    Private Sub ProcessItems(xOrderID As Long, xOrderTo As String)
        Dim SQL As String
        Dim lItemID As Long
        Dim lOrderID As Long
        Dim lProductDetailID As Long
        Dim clSOI As New clsDBO
        Dim cnt As Long
        SQL = "select * from shiporderitem "
        SQL = SQL + "where orderid = " & xOrderID
        clSOI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSOI.RowCount > 0 Then
            For cnt = 0 To clSOI.RowCount - 1
                clSOI.GetRowNo(cnt)
                lOrderID = clSOI.Fields("Orderid")
                lItemID = clSOI.Fields("itemid")
                lProductDetailID = GetProductDetailID(clSOI.Fields("ProductID"))
                If lProductDetailID > 0 Then
                    ProcessDetailID(lItemID, lOrderID, xOrderTo, lProductDetailID, clSOI.Fields("QTY"))
                End If
            Next
        End If
    End Sub

    Private Sub ProcessDetailID(xItemID As Long, xOrderID As Long, xOrderTo As String, xProductDetailID As Long, xQty As Long)
        Dim SQL As String
        Dim clSDI As New clsDBO
        Dim clSOI As New clsDBO
        Dim sSize As String
        Dim cnt As Long
        Dim lProductID As Long
        SQL = "select * from shipproductdetailitem "
        SQL = SQL + "where productdetailid = " & xProductDetailID
        clSDI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSDI.RowCount = 0 Then
            Exit Sub
        End If
        If xOrderTo = "0000" Then
            sSize = "Small"
        Else
            sSize = GetSize(xOrderTo)
        End If
        For cnt = 0 To clSDI.RowCount - 1
            clSDI.GetRowNo(cnt)
            lProductID = GetProductID(clSDI.Fields("productdetailitem"))
            SQL = "select * from shiporderitem "
            SQL = SQL + "where orderid = " & xOrderID & " "
            SQL = SQL + "and productid = " & lProductID
            clSOI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clSOI.RowCount > 0 Then
                clSOI.GetRow()
                If sSize = "Small" Then
                    clSOI.Fields("qty") = clSOI.Fields("qty") + (xQty * CLng(clSDI.Fields("smallqty")))
                End If
                If sSize = "Medium" Then
                    clSOI.Fields("qty") = clSOI.Fields("qty") + (xQty * CLng(clSDI.Fields("mediumqty")))
                End If
                If sSize = "Large" Then
                    clSOI.Fields("qty") = clSOI.Fields("qty") + (xQty * CLng(clSDI.Fields("largeqty")))
                End If

            Else
                clSOI.NewRow()
                clSOI.Fields("orderid") = xOrderID
                clSOI.Fields("productid") = lProductID
                If sSize = "Small" Then
                    clSOI.Fields("qty") = xQty * CLng(clSDI.Fields("smallqty"))
                End If
                If sSize = "Medium" Then
                    clSOI.Fields("qty") = xQty * CLng(clSDI.Fields("mediumqty"))
                End If
                If sSize = "Large" Then
                    clSOI.Fields("qty") = xQty * CLng(clSDI.Fields("largeqty"))
                End If
                clSOI.AddRow()
            End If
            clSOI.SaveDB()
        Next
        SQL = "delete shiporderitem where itemid = " & xItemID
        clSOI.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Function GetProductID(xProductDetailID As Long) As Long
        Dim SQL As String
        Dim clSP As New clsDBO
        GetProductID = 0
        SQL = "select * from shipproduct "
        SQL = SQL + "where productdetailid = " & xProductDetailID
        clSP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSP.RowCount > 0 Then
            clSP.GetRow()
            GetProductID = clSP.Fields("productid")
        End If
    End Function

    Private Function GetSize(xOrderTo As String) As String
        Dim SQL As String
        Dim clSize As New clsDBO
        GetSize = "Small"
        If xOrderTo.Substring(0, 1) = "A" Then
            SQL = "select * from shipagentlevel "
            SQL = SQL + "where agentno = '" & xOrderTo & "' "
            clSize.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clSize.RowCount > 0 Then
                clSize.GetRow()
                GetSize = clSize.Fields("agentlevel")
            End If
        Else
            SQL = "select * from shipdealerlevel "
            SQL = SQL + "where dealerno = '" & xOrderTo & "' "
            clSize.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clSize.RowCount > 0 Then
                clSize.GetRow()
                GetSize = clSize.Fields("dealerlevel")
            End If
        End If
    End Function

    Public Function GetProductDetailID(xProductID As Long) As Long
        Dim SQL As String
        Dim clSP As New clsDBO
        GetProductDetailID = 0
        SQL = "select * from shipproduct "
        SQL = SQL + "where productid = " & xProductID
        clSP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSP.RowCount > 0 Then
            clSP.GetRow()
            GetProductDetailID = clSP.Fields("productdetailid")
        End If
    End Function

    Private Sub chkOvernight_CheckedChanged(sender As Object, e As EventArgs) Handles chkOvernight.CheckedChanged
        Dim clSO As New clsDBO
        Dim SQL As String
        Dim lOrderID As Long
        lOrderID = rgOrder.SelectedValue
        SQL = "select * from shiporder "
        SQL = SQL + "where orderid = " & lOrderID & " "
        clSO.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSO.RowCount > 0 Then
            clSO.GetRow()
            clSO.Fields("overnight") = chkOvernight.Checked
            clSO.SaveDB()
        End If
    End Sub

    Private Sub btnSaveComment_Click(sender As Object, e As EventArgs) Handles btnSaveComment.Click
        Dim lOrderID As Long
        Dim clSO As New clsDBO
        Dim SQL As String
        lOrderID = rgOrder.SelectedValue
        SQL = "select * from shiporder "
        SQL = SQL + "where orderid = " & lOrderID & " "
        clSO.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSO.RowCount > 0 Then
            clSO.GetRow()
            clSO.Fields("comments") = txtComments.Text
            clSO.SaveDB()
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSubAgent_Click(sender As Object, e As EventArgs) Handles btnSubAgent.Click
        Response.Redirect("~/agents/SubAgentSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnFulFillment_Click(sender As Object, e As EventArgs) Handles btnFulFillment.Click
        Response.Redirect("~/fulfillment/fulfillment.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnFulfillmentStock_Click(sender As Object, e As EventArgs) Handles btnFulfillmentStock.Click
        Response.Redirect("~/fulfillment/fulfillmentstock.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfUserID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

End Class