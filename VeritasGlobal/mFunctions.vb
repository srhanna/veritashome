﻿Module mFunctions
    Public Function GetUserInfo(xUserID As Long) As String
        GetUserInfo = ""
        If xUserID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clUI As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & xUserID
        clUI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clUI.RowCount > 0 Then
            clUI.GetRow()
            GetUserInfo = clUI.Fields("fname") & " " & clUI.Fields("lname")
        End If

    End Function

    Public Function GetPayee(xID As Long) As String
        GetPayee = ""
        If xID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clP As New clsDBO
        SQL = "select * from payee "
        SQL = SQL + "where payeeid = " & xID
        clP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clP.RowCount > 0 Then
            clP.GetRow()
            GetPayee = clP.Fields("CompanyName")
        End If

    End Function

    Public Function GetSubAgentInfo(xID As Long) As String
        GetSubAgentInfo = ""
        If xID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clSA As New clsDBO
        SQL = "select * from subagents "
        SQL = SQL + "where subagentid = " & xID
        clSA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSA.RowCount > 0 Then
            clSA.GetRow()
            GetSubAgentInfo = clSA.Fields("subagentno") & " / " & clSA.Fields("subagentname")
        End If
    End Function

    Public Function GetAgentInfo(xID As Long) As String
        GetAgentInfo = ""
        If xID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & xID
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            GetAgentInfo = clA.Fields("agentno") & " / " & clA.Fields("agentname")
        End If
    End Function

    Public Function GetRateType(xID As Long) As String
        GetRateType = ""
        If xID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clRT As New clsDBO
        SQL = "select * from ratetype "
        SQL = SQL + "where ratetypeid = " & xID
        clRT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRT.RowCount > 0 Then
            clRT.GetRow()
            GetRateType = clRT.Fields("ratetypename")
        End If
    End Function
End Module
