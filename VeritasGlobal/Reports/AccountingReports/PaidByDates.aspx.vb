﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class PaidByDates
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
        End If

        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Dim SQL As String
        Dim clR As New clsDBO
        If rdpTo.SelectedDate Is Nothing Then
            Exit Sub
        End If
        If rdpFrom.SelectedDate Is Nothing Then
            Exit Sub
        End If
        SQL = "select contractno, fname + ' ' + lname as CustName, DatePaid, SaleDate, RetailRate, 
        MoxyDealerCost as DealerCost, dealerno, dealername, pt.plantype, termmonth, cr.amt as ClaimReserve, 
        cf.amt as ClipFee, pt2.amt as PremTax, a.amt as Admin, m.amt as Markup, comm.amt as Commission, crm.amt as crmamt from contract c 
        inner join dealer d on d.dealerid = c.dealerid 
        inner join plantype pt on pt.plantypeid = c.plantypeid 
        left join (select contractid, sum(amt) as Amt from contractamt 
        where ratetypeid = 1 group by contractid) cr 
        on cr.contractid = c.contractid 
        left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 2 group by ContractID) cf
		on cf.ContractID = c.ContractID 
        left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 3 group by ContractID) pt2
		on pt2.ContractID = c.ContractID 
        left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 4 group by ContractID) a
		on a.ContractID = c.ContractID 
        left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 5 group by ContractID) m
		on m.ContractID = c.ContractID 
		left join (select contractid, sum(amt) as Amt from contractamt ca
		inner join ratetype rt on rt.RateTypeID = ca.RateTypeID 
		where rt.RateCategoryID = 3 
		and ca.RateTypeID <> 1010 
		and ca.RateTypeID <> 2008 
		and ca.RateTypeID <> 2010
		group by contractid) comm
		on comm.ContractID = c.ContractID 
		left join (select contractid, sum(amt) as Amt from contractamt where (ratetypeid = 1010 or RateTypeID = 2008 or RateTypeID = 2010) group by ContractID) crm
		on crm.ContractID = c.ContractID "
        SQL = SQL + "where datepaid >= '" & rdpFrom.SelectedDate & "' "
        SQL = SQL + "and datepaid <= '" & rdpTo.SelectedDate & "' "
        rgPaid.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgPaid.Rebind()
    End Sub

    Private Sub rgPaid_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgPaid.ItemCommand
        Dim a As String
        a = e.CommandName
        If e.CommandName = "ExportToExcel" Then
            rgPaid.ExportSettings.ExportOnlyData = False
            rgPaid.ExportSettings.IgnorePaging = True
            rgPaid.ExportSettings.OpenInNewWindow = True
            rgPaid.ExportSettings.UseItemStyles = True
            rgPaid.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgPaid.MasterTableView.ExportToExcel()
        End If
    End Sub
End Class