﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class RatingVerification
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            FillGrid()
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub FillGrid()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select contractno, c.Status, MoxyDealerCost, CustomerCost, pt.PlanType, TermMonth, (cr.Amt) as ClaimReserve, (cp.amt) as ClipFee, 
        (tax.Amt) as PremiumTax, (a.Amt) as Admin, (m.Amt) as Markup, 
        case when (jd.Amt) is null then 0 else (jd.Amt) end  as JDecker,
        case when (mg.Amt) is null then 0 else (mg.Amt) end as mcgrp, 
        case when (gd.Amt) is null then 0 else (gd.Amt) end as GrandeCom, 
        case when (dmg.Amt) is null then 0 else (dmg.Amt) end as DirectMarketing, 
        case when (vsc.Amt) is null then 0 else (vsc.Amt) end as VSCLogistics,
        case when (f.Amt) is null then 0 else (f.Amt) end as ForteCRMFee, 
        case when (mcf.Amt) is null then 0 else (mcf.Amt) end as MoxyCRMFee, 
        case when (ap.amt) is null then 0 else (ap.amt) end as AutoProtectUSA,
        (MoxyDealerCost - case when (cr.Amt) is null then 0 else (cr.Amt) end - 
        case when (cp.amt - cp.CancelAmt) is null then 0 else (cp.amt) end - 
        case when (tax.Amt - tax.CancelAmt) is null then 0 else (tax.Amt) end - 
        case when (a.Amt) is null then 0 else (a.Amt) end - 
        case when (m.Amt) is null then 0 else (m.Amt) end -
        case when (jd.Amt) is null then 0 else (jd.Amt) end - 
        case when (mg.Amt) is null then 0 else (mg.Amt) end - 
        case when (gd.Amt) is null then 0 else (gd.Amt) end - 
        case when (dmg.Amt) is null then 0 else (dmg.Amt) end - 
        case when (vsc.Amt) is null then 0 else (vsc.Amt) end -
        case when (f.Amt) is null then 0 else (f.Amt) end - 
        case when (mcf.Amt) is null then 0 else (mcf.Amt) end - 
        case when (ap.amt) is null then 0 else (ap.amt) end) as DiffAmt
        from contract c
        inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1) cr on cr.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=2) cp on cp.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=3) tax on tax.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=4) a on a.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=5) m on m.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=6) jd on jd.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=7) mg on mg.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=8) gd on gd.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1008) dmg on dmg.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1009) vsc on vsc.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1010) f on f.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=2010) MCF on MCF.ContractID = c.ContractID
        left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=2014) AP on AP.ContractID = c.ContractID
        where ((MoxyDealerCost - case when (cr.Amt) is null then 0 else (cr.Amt) end - 
        case when (cp.amt) is null then 0 else (cp.amt) end - 
        case when (tax.Amt) is null then 0 else (tax.Amt) end - 
        case when (a.Amt) is null then 0 else (a.Amt) end - 
        case when (m.Amt) is null then 0 else (m.Amt) end -
        case when (jd.Amt) is null then 0 else (jd.Amt) end - 
        case when (mg.Amt) is null then 0 else (mg.Amt) end - 
        case when (gd.Amt) is null then 0 else (gd.Amt) end - 
        case when (dmg.Amt) is null then 0 else (dmg.Amt) end - 
        case when (vsc.Amt) is null then 0 else (vsc.Amt) end -
        case when (f.Amt) is null then 0 else (f.Amt) end - 
        case when (mcf.Amt) is null then 0 else (mcf.Amt) end - 
        case when (ap.amt) is null then 0 else (ap.amt) end) > 1
        or
        (MoxyDealerCost - case when (cr.Amt) is null then 0 else (cr.Amt) end - 
        case when (cp.amt) is null then 0 else (cp.amt) end - 
        case when (tax.Amt) is null then 0 else (tax.Amt) end - 
        case when (a.Amt) is null then 0 else (a.Amt) end - 
        case when (m.Amt) is null then 0 else (m.Amt) end -
        case when (jd.Amt) is null then 0 else (jd.Amt) end - 
        case when (mg.Amt) is null then 0 else (mg.Amt) end - 
        case when (gd.Amt) is null then 0 else (gd.Amt) end - 
        case when (dmg.Amt) is null then 0 else (dmg.Amt) end - 
        case when (vsc.Amt) is null then 0 else (vsc.Amt) end -
        case when (f.Amt) is null then 0 else (f.Amt) end - 
        case when (mcf.Amt) is null then 0 else (mcf.Amt) end - 
        case when (ap.amt) is null then 0 else (ap.amt) end) < -1) "

        rgPaid.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgPaid.Rebind()
    End Sub

    Private Sub rgPaid_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgPaid.ItemCommand
        Dim a As String
        a = e.CommandName
        If e.CommandName = "ExportToExcel" Then
            rgPaid.ExportSettings.ExportOnlyData = False
            rgPaid.ExportSettings.IgnorePaging = True
            rgPaid.ExportSettings.OpenInNewWindow = True
            rgPaid.ExportSettings.UseItemStyles = True
            rgPaid.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgPaid.MasterTableView.ExportToExcel()
        End If
    End Sub
End Class